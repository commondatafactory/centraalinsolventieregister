package store

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/commondatafactory/centraalinsolventieregister/soap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"github.com/Attumm/settingo/settingo"
)

func syncDatabase() {
	// check reports presence for last six months.
	// get changes per day
	// get/store details per case
	LoadDays(182)
	lookupInsolvencyIDs()
	lookupCases()
}

// Load publications from past x number of days.
func LoadDays(days int) {

	today := time.Now()

	pastStart := settingo.GetInt("paststart")

	for i := pastStart; i < (days + pastStart); i++ {
		from := today.Add(time.Duration(i+1) * -24 * time.Hour)
		to := today.Add(time.Duration(i) * -24 * time.Hour)
		fmt.Printf("Working on %s %s \n", from.Format("2006-02-01"), to.Format("2006-02-01"))
		reports := soap.GetLastupdateSince(from, to)
		storeReports(reports)
	}
}

// Look for last publication date of reports
// lookupnew Reports on a day by day basis.
// lookup InsolvencyIDs
// with InsolvencyIDs lookup new/updated Cases.
func Update() {

	r := Report{}
	r.LastPublication()

	if r.Insolvencynumber == "" {
		log.Println("no publications present. lets do a full synchronization with source")
		syncDatabase()
	}

	stop := time.Now()
	stop = stop.Add(24 * time.Hour)

	for from := r.Publication; from.Before(stop); from = from.Add(24 * time.Hour) {
		to := from.Add(24 * time.Hour)

		fmt.Printf("Working on %s %s \n", from.Format("2006-02-01"), to.Format("2006-02-01"))

		reports := soap.GetLastupdateSince(from, to)
		storeReports(reports)
	}

	// lookup required ids
	lookupInsolvencyIDs()
	// lookup missing cases
	lookupCases()
	// lookup all mentioned addresses
	ExtractAllLocations()
}

// For publications lookup the insolvencies
func lookupInsolvencyIDs() {

	var missingids []Report

	DB.Where(`insolvency_id = ''`).Find(&missingids)
	fmt.Printf("finding missing insolvency ids: %d \n", len(missingids))

	for i := range missingids {

		report := missingids[i]
		courtCode := soap.CourtIDs[report.Court]
		insolvency := report.Insolvencynumber
		pubcodes := soap.SearchInsolvencyID(insolvency, courtCode)

		if len(pubcodes) == 0 {
			continue
		}

		// store publication / insolvency code. on report.
		DB.Model(&report).Update("insolvency_id", pubcodes[0].Value)

		// print an update now and then.
		if i%50 == 0 {
			fmt.Printf("working on %d\n", i)
		}
	}
}

// lookUpCases downloads the actual cases from soap api.
// if not already downloaded before.
func lookupCases() {

	var missingcases []Report

	fmt.Println("lookup missing cases")
	// We look for insolvency_id not being present in the raw_case table
	// where the created at of report should be older than the insolvencycase.
	// If there has been an update we force to download a new case.
	tx := DB.Model(&Report{})

	tx.Joins(`
		join lateral (
		  select id, created_at, updated_at
		  from cir.raw_case rc
		  where rc.insolvency_id = cir.reports.insolvency_id
		  union --empty row for no match
		  select null, cir.reports.created_at, null
		  order by created_at desc limit 1) c on true`)
	tx.Where(`c.id is null OR c.updated_at < (cir.reports.updated_at - interval '1 hour')`)
	result := tx.Find(&missingcases)

	if result.Error != nil {
		panic(result.Error)
	}

	fmt.Printf("downloading complete insolvency cases: %d \n", len(missingcases))

	for i := range missingcases {
		lookupSingleCase(missingcases[i])
		// print an update now and then.
		if i%50 == 0 {
			fmt.Printf("working on %d of %d \n", i, len(missingcases))
		}

	}

}

// lookupCaseCache looks up a case at soap api using insolvencyId
// stores raw response xml
func lookupSingleCase(r Report) {

	_, xml, err := soap.GetCaseWithReports(r.InsolvencyID)

	if err != nil {
		fmt.Println(err)
		return
	}

	dbCase := RawCase{
		InsolvencyID: r.InsolvencyID,
		Publication:  r.Publication,
		XML:          xml,
	}

	dbCase.SetJSON()
	DB.Create(&dbCase)
}

// Fill / update json field for cases
// into seperate database table to possible create views.
func ExtractInsolvencyPublications() {

	var cases []RawCase

	// look for empty json fields.
	DB.Model(&RawCase{}).Find(&cases)

	for i := range cases {
		cases[i].SetJSON()
		DB.Save(cases[i])
	}
}

func ExtractAllLocations() {

	var cases []RawCase
	locations := []InsolvencyLocation{}

	DB.Model(&RawCase{}).Find(&cases)

	errors := 0

	for i := range cases {
		ocr := cases[i].ParseXML()
		insolv_locs := ocr.AllAddresses()

		for ii := range insolv_locs {
			nl := InsolvencyLocation{}
			err := nl.LoadSoap(insolv_locs[ii])
			if err != nil {
				fmt.Println(err)
				errors += 1
				continue
			}
			nl.Publication = cases[i].Publication
			nl.ReportPublication = cases[i].ReportPublication
			nl.CaseType = cases[i].CaseType
			nl.Description = cases[i].Description
			nl.InsolvencyID = cases[i].InsolvencyID
			locations = append(locations, nl)
		}
	}

	DB.CreateInBatches(locations, 100)
	InsolvencyLocation{}.cleanup()
	fmt.Printf("adres errors %d ", errors)

	// merge locations with bag to create a map
	mergeLocationWithBAG()
}

// merge locations with BAG data. Needs specif prepared
// BAG database to do this. disabled by default.
func mergeLocationWithBAG() {

	if !settingo.GetBool("MERGEBAG") {
		return
	}

	DB.Exec(`drop table if exists cir.insolvency_locations_resolved_new;`)

	result := DB.Exec(`
select
	numinsolvencies.*,
	ST_SetSRID(ST_Transform(numinsolvencies.geopunt, 3857), 3857) as point,
	nbwgp.buurtcode,
	nbwgp.buurtnaam,
	nbwgp.wijkcode,
	nbwgp.wijknaam,
	nbwgp.gemeentenaam,
	nbwgp.gemeentecode,
	nbwgp.identificatie as provinciecode,
	nbwgp.naam as provincienaam
into cir.insolvency_locations_resolved_new
from (
	select
	    b.huisletter, b.huisnummertoevoeging, il.*,
	    b.numid, b.pid, b.sid, b.lid,
	    b.geopunt,
	    levenshtein(
	       concat(lower(huisletter) , lower(huisnummertoevoeging)),
	       concat(lower(addition1), lower(addition2))
	    ) toevoeging_score
	from cir.insolvency_locations il,
	lateral ( select
			an.numid, an.pid, an.sid, an.lid,
			an.huisletter, an.huisnummertoevoeging,
			an.geopunt
	          from baghelp.all_pvsl_nums an
	          where an.postcode = il.postal_code
	          and an.huisnummer = il.street_number
	          order by levenshtein(
		    concat(lower(an.huisletter) , lower(an.huisnummertoevoeging)),
		    concat(lower(il.addition1), lower(il.addition2))
		  ) limit 1
	) b
) numinsolvencies
left outer join baghelp.num_buurt_wijk_gemeente_provincie nbwgp on nbwgp.numid = numinsolvencies.numid;
	`)

	if result.Error != nil {
		panic(result.Error)
	}

	// rowsAffected := result.RowsAffected
	fmt.Printf("locations resolved %d \n", result.RowsAffected)

	DBExec(`ALTER TABLE cir.insolvency_locations_resolved_new ALTER COLUMN POINT TYPE geometry(Point,3857);`)

	DBExec(`create index on cir.insolvency_locations_resolved_new using gist (point);`)
	DBExec(`create index on cir.insolvency_locations_resolved_new (insolvency_id);`)

	DBExec(`drop view if exists cir.insolvency_locations_resolved_epoch;`)
	DBExec(`drop table if exists cir.insolvency_locations_resolved;`)

	DBExec(`set search_path to cir;
		 alter table insolvency_locations_resolved_new rename to insolvency_locations_resolved;`)

	DBExec(`
		create view cir.insolvency_locations_resolved_epoch as (
		select *, extract(epoch from ilr.publication) pt
		from cir.insolvency_locations_resolved ilr);`)

	fmt.Printf("new locations table created")
}

// remove old / duplicate data
func Clean() {
	fmt.Printf("cleaning up database tables")
	Report{}.cleanup()
	RawCase{}.cleanup()
	InsolvencyLocation{}.cleanup()
}

// every x seconds print counts
func showStatusLine() {
	for {
		status()
		time.Sleep(5 * time.Second)
	}
}

// create status line with database counts.
func StatusLine() string {

	var count_reports, count_locations int64
	var count_distinct_insolv_cases int64

	DB.Model(&Report{}).Count(&count_reports)
	DB.Model(&InsolvencyLocation{}).Count(&count_locations)
	DB.Model(&RawCase{}).Select("count(distinct(insolvency_id))").Count(&count_distinct_insolv_cases)

	fmt.Printf("Reports: %d, Insolvencies: %d \n",
		count_reports,
		count_distinct_insolv_cases,
	)

	oldest := Report{}
	newest := Report{}

	oldest.OldestPublication()
	newest.LastPublication()

	msg := fmt.Sprintf("oldest: %s newest %s  historicdays: %.1f days. %d locations\n",
		oldest.Publication,
		newest.Publication,
		time.Since(oldest.Publication).Hours()/24,
		count_locations,
	)
	return msg
}

// status show status line with database counts.
func status() {
	fmt.Println(StatusLine())
}

func DefaultSettings() {
	settingo.SetBool("debug", false, "debug information during run")

	settingo.Set("SOAPUSER", "", "cir username")
	settingo.Set("SOAPPASSWORD", "", "cir password")

	settingo.SetBool("parsecases", false, "parse stored xml data")
	settingo.SetBool("extractlocations", false, "parse stored xml data")
	settingo.SetBool("testsoap", false, "Do a simple soap request to get last update.")
	settingo.SetBool("update", false, "update all tables with new cases since last time")
	settingo.SetBool("sync", false, "Do a 6 month sync")
	settingo.SetInt("paststart", 0, "days in the past start loading")
	settingo.SetInt("pastdays", 0, "days in the past loading")
	settingo.SetBool("lookupids", false, "lookup insolvency ids in soap api for reports")
	settingo.SetBool("lookupcases", false, "using insolvency ids lookup actual cases")

	settingo.SetBool("clean", false, "delete old / duplicate data")

	settingo.SetBool("MERGEBAG", false, "merge locations with bag database")

	settingo.Set("PGDATABASE", "cdf", "database name")
	settingo.Set("PGHOST", "127.0.0.1", "database name")
	settingo.Set("PGPORT", "5432", "database name")
	settingo.Set("PGUSER", "cdf", "database user")
	settingo.Set("PGPASSWORD", "insecure", "db password")
	settingo.Set("SSLMODE", "require", "sslmode require / disable")

	settingo.SetBool("status", true, "print some database statistics")
}

var DB *gorm.DB

func SetupDB() {

	dsn := fmt.Sprintf(
		`host=%v user=%v password=%v dbname=%v port=%v sslmode=%v`,
		settingo.Get("PGHOST"),
		settingo.Get("PGUSER"),
		settingo.Get("PGPASSWORD"),
		settingo.Get("PGDATABASE"),
		settingo.Get("PGPORT"),
		settingo.Get("SSLMODE"),
	)

	if settingo.GetBool("debug") {
		fmt.Println(dsn)
	}

	var err error

	if settingo.GetBool("debug") {
		DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
	} else {
		DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
			// Logger: logger.Default.LogMode(logger.Info),
		})

	}

	if err != nil {
		panic(err)
	}

	// creates tables
	err = DB.AutoMigrate(&Report{}, &RawCase{}, &InsolvencyLocation{})

	if err != nil {
		panic(err)
	}
}

func Start() {

	DefaultSettings()
	settingo.Parse()
	SetupDB()

	go showStatusLine()

	if settingo.GetBool("testsoap") {
		date, err := soap.GetLastupdate()
		if err != nil {
			panic("soap connection to CIR register failed")
		}
		fmt.Println(date)
		return
	}

	if settingo.GetBool("sync") {
		syncDatabase()
		return
	}

	if settingo.GetBool("clean") {
		Clean()
		return
	}

	if settingo.GetBool("parsecases") {
		ExtractInsolvencyPublications()
		return
	}
	if settingo.GetBool("extractlocations") {
		ExtractAllLocations()
		return
	}

	pastdays := settingo.GetInt("pastdays")

	if pastdays < 0 || pastdays > 190 {
		panic("pastdays has invalid number. 0 > good < 190")
	}
	if pastdays != 0 {
		LoadDays(pastdays)
	}

	if settingo.GetBool("lookupids") {
		lookupInsolvencyIDs()
	}

	if settingo.GetBool("lookupcases") {
		lookupCases()
	}

	// update latest reports and cases.
	if settingo.GetBool("update") {
		Update()
		Clean()
	}

	if settingo.GetBool("status") {
		status()
	}
}
