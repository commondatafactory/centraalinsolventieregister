package store

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/commondatafactory/centraalinsolventieregister/soap"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Report struct {
	gorm.Model
	Insolvencynumber string
	Court            string
	Publication      time.Time
	// PublicationReport time.Time
	InsolvencyCode string
	Title          string
	EndReport      string
	InsolvencyID   string
}

func (r *Report) TableName() string {
	return "cir.reports"
}

func (r *Report) LastPublication() {
	DB.Order("publication desc").First(r)
}

func (r *Report) OldestPublication() {
	DB.Order("publication asc").First(r)
}

// removeDuplicateReports removes invalid records
// and older then 6 monhts.
func (r Report) cleanup() {
	rowsAffected := DBExec(`
	delete from cir.reports where id in (
	select id from (
		select *,
			   RANK() over ( partition by insolvencynumber, court order by created_at desc) dupes
		from cir.reports r
	) addrank
	where dupes > 1
	OR created_at <  NOW() - interval '6 month'
	OR publication < NOW() - interval '6 month'
	)
	`).RowsAffected

	fmt.Printf("Reports deleted %d \n", rowsAffected)
}

func storeReports(r []soap.Report) {

	reports := []*Report{}

	for sri := range r {
		sr := r[sri]

		newR := &Report{
			Insolvencynumber: sr.Insolventienummer.Value,
			Court:            sr.Rechtbank.Value,
			Publication:      sr.Publicatie.Parse(),
			InsolvencyCode:   sr.Kenmerk.Value,
			Title:            sr.Titel.Value,
			EndReport:        sr.Eindverslag.Value,
			// InsolvencyID is updated later.
		}
		reports = append(reports, newR)
	}
	DB.CreateInBatches(reports, 10)

	// removes older stuff
	Report{}.cleanup()
}

type RawCase struct {
	gorm.Model
	InsolvencyID      string `gorm:"index"`
	Publication       time.Time
	ReportPublication time.Time
	CaseType          int
	Description       string
	XML               string
	JSON              datatypes.JSON
}

func (r *RawCase) TableName() string {
	return "cir.raw_case"
}

// Execute Database operations and panic on error!!
func DBExec(sql string) *gorm.DB {

	result := DB.Exec(sql)
	if result.Error != nil {
		fmt.Println(sql)
		panic(result.Error)
	}
	return result
}

func (r RawCase) cleanup() {
	// make sure every raw case knows about publication date.
	// of latest report about that case.
	rowsAffected := DBExec(`
	update cir.raw_case orc
	set report_publication = (
		select r."publication"
		from cir.raw_case rc
		left outer join cir.reports r on r.insolvency_id = rc.insolvency_id
		where rc.id=orc.id and r."publication"  is not null
		order by "publication" desc limit 1
	) where orc.report_publication is null
	`).RowsAffected

	fmt.Printf("raw cases with missing publication data: %d \n", rowsAffected)

	DB.Unscoped().Where(`created_at < NOW() - interval '6 month'`).Delete(&RawCase{})
	DB.Unscoped().Where(`report_publication < NOW() - interval '6 month'`).Delete(&RawCase{})

	dupes := DBExec(`
	delete from cir.raw_case where id in (
		select id from (
			select *,
			   RANK() over ( partition by insolvency_id order by id desc) dupes
			from cir.raw_case r
		) addrank
	where dupes > 1
	)`).RowsAffected

	fmt.Printf("Insolvency cases deduped: %d \n", dupes)
}

func (r *RawCase) ParseXML() soap.OperationGetCaseReport {

	out := soap.OperationGetCaseReport{}

	err := out.ParseXML([]byte(r.XML))

	if err != nil {
		fmt.Println(err)
	}

	return out
}

// convert xml to JSON and set data fields:
// CaseType, Publication date a json version of the xml
// Description of the case.
func (r *RawCase) SetJSON() {
	insolvency := r.ParseXML()
	pubs := insolvency.Publications()
	if len(pubs) > 0 {
		r.Publication = pubs[0].Date.ParseShort()
		r.CaseType = pubs[0].CodeType.Number()
		r.Description = pubs[0].Description.Value
	}
	insjson, _ := json.MarshalIndent(insolvency.Insolvency(), "", "  ")
	r.JSON = insjson
}

type InsolvencyLocation struct {
	gorm.Model
	InsolvencyID string `gorm:"index"`

	Publication       time.Time
	ReportPublication time.Time
	CaseType          int
	Description       string

	Street       string
	StreetNumber int
	Addition1    string
	Addition2    string
	PostalCode   string `gorm:"index"`
	Town         string

	Name      string
	KvkNumber string

	Details datatypes.JSON
}

func (r *InsolvencyLocation) TableName() string {
	return "cir.insolvency_locations"
}

func (l *InsolvencyLocation) LoadSoap(sl *soap.InsolvencyLocation) error {

	if sl.Adres == nil {
		return errors.New("missing adres")
	}

	if sl.Adres.Street == nil {
		return errors.New("missing street")
	}

	if sl.Adres.StreetNumber == nil {
		return errors.New("missing streetnumber")
	}

	if sl.Adres.Addition1 != nil {
		l.Addition1 = sl.Adres.Addition1.Value
	}

	if sl.Adres.Addition2 != nil {
		l.Addition2 = sl.Adres.Addition2.Value
	}

	if sl.Adres.PostalCode == nil {
		return errors.New("missing postalcode")
	}

	if sl.Adres.Town == nil {
		fmt.Println("missing town")
	} else {
		l.Town = sl.Adres.Town.Value
	}

	number := sl.Adres.StreetNumber.Number()

	l.Street = sl.Adres.Street.Value
	l.StreetNumber = number
	l.PostalCode = sl.Adres.PostalCode.Value

	if sl.Person != nil {
		insjson, _ := json.MarshalIndent(sl.Person, "", "  ")
		l.Details = insjson

		if sl.Person.Name != nil {
			l.Name = sl.Person.Name.Value
		}
		if sl.Person.KvKNumber != nil {
			l.KvkNumber = sl.Person.KvKNumber.Value
		}

	}
	if sl.TradingName != nil {
		insjson, _ := json.MarshalIndent(sl.TradingName, "", "  ")
		l.Details = insjson

		if sl.TradingName.Name != nil {
			l.Name = sl.TradingName.Name.Value
		}
		if sl.TradingName.KvKNumber != nil {
			l.KvkNumber = sl.TradingName.KvKNumber.Value
		}
	}

	return nil
}

func (l InsolvencyLocation) cleanup() {
	// cleanup locations when there are more cases on a single adres
	// with similar name, kvk etc.
	dupes := DBExec(`
	delete from cir.insolvency_locations where id in (
		select id from (
			select *,
			   RANK() over ( partition by insolvency_id, name, kvk_number, street, street_number, postal_code order by -id) dupes
			from cir.insolvency_locations
		) addrank
	where dupes > 1
	)`).RowsAffected

	fmt.Printf("locations deduped: %d \n", dupes)

	var oldlocations []InsolvencyLocation

	// We look for insolvency_id not being present in the raw_case table
	// or where the created_at of location should be newer than the
	// original insolvencycase.
	tx := DB.Model(&InsolvencyLocation{})
	tx.Joins(`
		left outer join cir.raw_case rc
		on (rc.insolvency_id = cir.insolvency_locations.insolvency_id
		    and cir.insolvency_locations.created_at > rc.created_at)`)
	tx.Where(`rc.id is null`)

	result := tx.Find(&oldlocations)

	if result.Error != nil {
		panic(result.Error)
	}

	if len(oldlocations) > 0 {
		DB.Unscoped().Delete(&oldlocations)
	}

	fmt.Printf("old locations deleted: %d \n", len(oldlocations))
}
