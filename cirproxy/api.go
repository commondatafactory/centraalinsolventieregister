package cirproxy

import (
	"fmt"
	"log"
	"net/http"

	"github.com/Attumm/settingo/settingo"
	"gitlab.com/commondatafactory/centraalinsolventieregister/store"
)

// Colors are fun, and can be used to note that this is joyfull and fun project.
const (
	// InfoColor    = "\033[1;34m%s\033[0m"
	// NoticeColor  = "\033[1;36m%s\033[0m"
	// WarningColor = "\033[1;33m%s\033[0m"
	// ErrorColor   = "\033[1;31m%s\033[0m"
	// DebugColor   = "\033[0;36m%s\033[0m"
	InfoColorN = "\033[1;34m%s\033[0m\n"
	// NoticeColorN  = "\033[1;36m%s\033[0m\n"
	// WarningColorN = "\033[1;33m%s\033[0m\n"
	// ErrorColorN   = "\033[1;31m%s\033[0m\n"
	// DebugColorN   = "\033[0;36m%s\033[0m\n"
)

func defaultSettings() {
	settingo.Set("http_host", "0.0.0.0:8000", "host with port")
	settingo.Set("CORS", "y", "CORS enabled")
}

func setupHandler() http.Handler {
	fmt.Println("Starting Centraal Insolventie Register API proxy.")
	mux := http.NewServeMux()
	// mux.Handle("/", http.FileServer(http.Dir("./files/www")))
	mux.HandleFunc("/lastupdate/", lastUpdate)
	mux.HandleFunc("/listcases/", listCases)
	mux.HandleFunc("/listcasesconvenient/", listConvenientCases)
	// mux.HandleFunc("/search/", searchRest)
	// mux.HandleFunc("/typeahead/", typeAheadRest)
	mux.HandleFunc("/status/", statusRest)
	mux.HandleFunc("/listcourts/", listCourts)
	mux.HandleFunc("/listcasecodes/", listCaseCodes)
	mux.HandleFunc("/help/", helpRest)
	mux.HandleFunc("/", helpRest)
	return mux
}

func Start() {
	defaultSettings()
	store.DefaultSettings()
	settingo.Parse()
	store.SetupDB()

	handler := setupHandler()

	ipPort := settingo.Get("http_host")
	cors := settingo.Get("CORS") == "y"
	middleware := MIDDLEWARE(cors)

	msg := fmt.Sprint(
		"setup http api proxy handler:",
		" port", settingo.Get("http_host"),
		" CORS: ", cors)

	fmt.Printf(InfoColorN, msg)
	log.Fatal(http.ListenAndServe(ipPort, middleware(handler)))
}
