package cirproxy

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"sync"
	"time"

	"gitlab.com/commondatafactory/centraalinsolventieregister/soap"
	"gitlab.com/commondatafactory/centraalinsolventieregister/store"
)

// single item map lock when updating new items
var lock = sync.RWMutex{}

// extractAPIcredentials find CIR API basic auth credentials.
func extractAPIcredentials(r *http.Request) {
	// TODO
}

func corsEnabled(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		origin := r.Header.Get("Origin")
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Expose-Headers", "*")

		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "GET,POST")
			w.Header().Set("Access-Control-Allow-Headers", "Page, Page-Size, Total-Pages, query, Total-Items, Query-Duration, Content-Type, X-CSRF-Token, Authorization")
			return
		} else {
			extractAPIcredentials(r)
			// make sure items are not being modified during request
			// otherwise wait..
			lock.RLock()
			defer lock.RUnlock()
			h.ServeHTTP(w, r)
		}
	})

}
func passThrough(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// make sure items / data not not being modified during request
		// otherwise wait..
		extractAPIcredentials(r)
		lock.RLock()
		defer lock.RUnlock()
		h.ServeHTTP(w, r)
	})
}

func MIDDLEWARE(cors bool) func(http.Handler) http.Handler {

	if cors {
		return corsEnabled
	}

	return passThrough
}

func lastUpdate(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	response := make(map[string][]string)
	date, err := soap.GetLastupdate()

	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		response["error"] = []string{err.Error()}
	} else {

		w.WriteHeader(http.StatusOK)
		response["lastupdate"] = []string{date.Format("2006-01-02")}
	}
	json.NewEncoder(w).Encode(response)

}

func listCourts(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	response := make(map[string]map[string]string)

	response["courts"] = make(map[string]string)

	names := []string{}
	for name := range soap.CourtIDs {
		names = append(names, name)
	}
	sort.Strings(names)

	for _, k := range names {
		response["courts"][k] = soap.CourtIDs[k]
	}

	json.NewEncoder(w).Encode(response)
}

func listCaseCodes(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	response := make(map[string]map[string]string)

	response["casetypes"] = make(map[string]string)

	codes := []string{}

	for c := range soap.CaseCodes {
		codes = append(codes, c)
	}
	sort.Strings(codes)

	for _, k := range codes {
		response["casetypes"][k] = soap.CaseCodes[k]
	}

	json.NewEncoder(w).Encode(response)
}

func statusRest(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	response := make(map[string]string)

	response["status"] = store.StatusLine()
	json.NewEncoder(w).Encode(response)
}

func helpRest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	response := make(map[string][]string)

	host := r.Host

	response["examples"] = []string{
		fmt.Sprintf("Convenience API proxying content available at https://insolventies.rechtspraak.nl/"),
		fmt.Sprintf("lastupdate https://ciruser:cirpassword@%s/lastupdate/", host),
		fmt.Sprintf("listcourts https://%s/listcourts/", host),
		fmt.Sprintf("listcasecodes https://%s/listcasecodes/", host),
		"1 on 1 copy of data at rechtspraak.nl",
		fmt.Sprintf("listcases https://%s/listcases/", host),
		fmt.Sprintf("listcases https://%s/listcases/?insolvency_id=YOURID", host),

		"cases with usefull extracted information:",
		fmt.Sprintf("listcases https://%s/listcasesconvenient/", host),
		fmt.Sprintf("listcases https://%s/listcasesconvenient/?insolvency_id=YOURID", host),

		fmt.Sprintf("help https://%s/help/", host),

		"insolvencies on map: tilesserver: https://tileserver.commondatafactory.nl/cir.insolvency_locations_resolved_epoch.html",
	}

	response["registration user / password"] = []string{
		"go to: https://insolventies.rechtspraak.nl/#!/registratie",
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func handleQueryError(err error, status int, w http.ResponseWriter) {
	response := make(map[string]string)
	w.WriteHeader(status)
	response["error"] = err.Error()
	json.NewEncoder(w).Encode(response)
}

type CaseResponse struct {
	OffsetID          uint
	InsolvencyID      string `gorm:"index"`
	Publication       time.Time
	ReportPublication time.Time
	CaseType          int
	Description       string
	Insolvency        soap.OperationGetCaseReport
}

func listCases(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	err := r.ParseForm()

	if err != nil {
		handleQueryError(err, http.StatusBadRequest, w)
	}

	limit := 100
	offset := 0

	idStr, caseIdGiven := r.Form["insolvency_id"]
	limitStr, limitGiven := r.Form["limit"]
	offsetStr, offsetGiven := r.Form["offset"]

	if limitGiven {
		limit, _ = strconv.Atoi(limitStr[0])
	}

	if offsetGiven {
		offset, _ = strconv.Atoi(offsetStr[0])
	}

	cases := []store.RawCase{}

	response := make(map[string][]CaseResponse)

	if caseIdGiven {
		insolvencyId := idStr[0]
		store.DB.Where("insolvency_id = ?", insolvencyId).Find(&cases)
	} else {
		store.DB.Limit(limit).Where("id > ?", offset).Order("id asc").Find(&cases)
	}

	for c := range cases {
		cr := CaseResponse{
			OffsetID:          cases[c].ID,
			InsolvencyID:      cases[c].InsolvencyID,
			Publication:       cases[c].Publication,
			ReportPublication: cases[c].ReportPublication,
			CaseType:          cases[c].CaseType,
			Description:       cases[c].Description,
			Insolvency:        cases[c].ParseXML(),
		}

		response["cases"] = append(response["cases"], cr)
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func listConvenientCases(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	err := r.ParseForm()

	if err != nil {
		handleQueryError(err, http.StatusBadRequest, w)
	}

	limit := 100
	offset := 0

	idStr, caseIdGiven := r.Form["insolvency_id"]
	limitStr, limitGiven := r.Form["limit"]
	offsetStr, offsetGiven := r.Form["offset"]

	if limitGiven {
		limit, _ = strconv.Atoi(limitStr[0])
	}

	if offsetGiven {
		offset, _ = strconv.Atoi(offsetStr[0])
	}

	cases := []store.InsolvencyLocation{}

	if caseIdGiven {
		insolvencyId := idStr[0]
		store.DB.Where("insolvency_id = ?", insolvencyId).Find(&cases)
	} else {
		store.DB.Limit(limit).Where("id > ?", offset).Order("id asc").Find(&cases)
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(cases)
}
