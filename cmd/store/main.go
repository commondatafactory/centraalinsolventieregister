package main

import (
	"gitlab.com/commondatafactory/centraalinsolventieregister/store"
)

func main() {
	store.Start()
}
