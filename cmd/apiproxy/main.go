package main

import (
	"gitlab.com/commondatafactory/centraalinsolventieregister/cirproxy"
)

func main() {
	cirproxy.Start()
}
