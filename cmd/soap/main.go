package main

import (
	"fmt"
	"time"

	"github.com/Attumm/settingo/settingo"
	"gitlab.com/commondatafactory/centraalinsolventieregister/soap"
)

func main() {
	settingo.Set("SOAPUSER", "", "cir username")
	settingo.Set("SOAPPASSWORD", "", "cir password")

	settingo.Parse()

	date, _ := soap.GetLastupdate()
	fmt.Println(date)

	// list new stuff from - to.
	soap.GetLastupdateSince(time.Now().Add(-24*time.Hour), time.Now())

	// lookup the insolvencies per court.
	soap.SearchInsolvencyID("F.01/20/183", "48")

	// load specific details of a case.
	// soap.GetCaseWithReports("01.obr.20.183.F.1325.1.21")

	// TODO. Remove old cruft. (6 months old)
	// soap.RemovedSince(time.Now().Add(-24*time.Hour))

}
