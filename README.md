# Centraal Insolventieregister

Golang client for the centraal insolventieregister / central insolvency register
Deals with the CIR SOAP API synchronizing data.
So you do not have to go through the same SOAP pain.

1) Do soap calls. Requires a user name and password
which is freely available.
Registration for username password:

https://insolventies.rechtspraak.nl/#!/registratie

2) Store returned data in postgres database no longer than 6 months.
   2.1) extract related addresses to cases so they can be geo-located / mapped.

3) Provide a REST proxy api (WIP)


The `cmd` folder contains executables

- apiproxy: provides an rest proxy API (WIP).
- soap: example code to use soap API.
- store: fully syncs 6 months of data.

Implements SOAP API calls.

If you have an empty database


1) Set environmet variables / settings.

    ```
    // rechtspraak acccount details
    SOAPPASSWORD
    SOAPUSER

    // fix/clean addresses.
    MERGEBAG

    // database settings
    PGPORT
    PGHOST
    PGDBATABASE
    PGUSER
    PGPASSWORD
    ```


2) Fetch all insolvency reports:


   ` cd cmd/store`

   ` ./store -h   # to see al options.`

   Sync to a full database:


    ```
    ./store -sync true

    ./store -update true   # get updates from cir since last update/sync.

    ./store -clean true    # remove old (6+ months) of data
    ```

# Data storage.

The following tables are created

- `reports`  containing publication details / codes to retrieve actual cases.
- `raw_case`  insolvency case which is stored as raw_case with an original xml and json representation derived from the xml.
- `insolvency_locations` addresses related to insolvency cases. extracted from raw cases.

# dependencies

- Needs baghelp.all_pvls_nums
- Needs baghelp.num_buurt_wijk_gemeente_provincie

They contain all BAG addresses and addresses coupled to administrative boundaries.

# Deployment

K8s Deployment example in /deployments/cdf-cir.yaml

# Compliancy NOTES

Insolvency data is stored at most 6 months as is allowed by law.

# Thanks / Inspiration

a working xml soap message was found at

   https://github.com/StichtingOpenGeo/CentraalInsolventieregister thanks skinkie!

Still quite some labour was needed to make the CIR API usable.
