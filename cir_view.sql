SELECT "reports"."id","reports"."created_at","reports"."updated_at","reports"."deleted_at",
"reports"."insolvencynumber","reports"."court","reports"."publication","reports"."publication_report","reports"."insolvency_code",
,"reports"."end_report","reports"."insolvency_id" FROM "cir"."reports"
		join lateral (
		  select id, created_at, updated_at
		  from cir.raw_case rc
		  where rc.insolvency_id = cir.reports.insolvency_id
		  order by rc.updated_at desc limit 1) c on true WHERE (c.id is null OR c.updated_at < (cir.reports.updated_at - interval '1 hour')) AND "reports"."deleted_at" IS null


select * from "cir"."reports"
		join lateral (
		  select id, created_at, updated_at
		  from cir.raw_case rc
		  where rc.insolvency_id = cir.reports.insolvency_id
		 union  -- empty row for no match.
		  select null, cir.reports.created_at, null
		  order by updated_at desc limit 1
		  ) c on true  WHERE (c.id is null OR c.updated_at < (cir.reports.updated_at - interval '1 hour')) AND cir.reports."deleted_at" IS null

select
	pand.*,
	cdf.ST_Transform(pbwgp.point, 3857) as point,
	pbwgp.buurtcode,
	pbwgp.buurtnaam,
	pbwgp.wijkcode,
	pbwgp.wijknaam,
	pbwgp.gemeentenaam,
	pbwgp.gemeentecode,
	pbwgp.provinciecode,
	pbwgp.provincienaam
into cir.insolvency_locations_resolved_new
from (
	select b.huisletter, b.huisnummertoevoeging, il.*, b.pid
	       , levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), concat(lower(addition1), lower(addition2))) toevoeging_score
	from cir.insolvency_locations il,
	lateral ( select pa.pid, pa.huisletter, pa.huisnummertoevoeging
	          from baghelp.pand_vbo_nums_labels pa
	          where pa.postcode = il.postal_code
	          and pa.huisnummer = il.street_number
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), concat(lower(il.addition1), lower(il.addition2))) limit 1
	) b
) pand
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pbwgp on pbwgp.pid = pand.pid;

ALTER TABLE "cir"."insolvency_locations_resolved_new" RENAME TO "cir"."insolvency_locations_resolved";


create view cir.insolvency_locations_resolved_epoch as (
 	select *, extract(epoch from ilr.publication) pt
	from cir.insolvency_locations_resolved ilr
)
