package soap

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"regexp"
	"time"

	"github.com/Attumm/settingo/settingo"
)

// Message is an opaque type used by the RoundTripper to carry XML
// documents for SOAP.
type Message interface{}

// HTTPError is detailed soap http error
type HTTPError struct {
	StatusCode int
	Status     string
	Msg        string
}

func (e *HTTPError) Error() string {
	return fmt.Sprintf("%q: %q", e.Status, e.Msg)
}

// SoapRequest Send a crafted xml soapRequest buffer to url of
// soapservice teruns bytes array repsone. Debugs and prints exceptions.
func SoapRequest(url string, soapRequest bytes.Buffer) ([]byte, error) {

	req, err := http.NewRequest("POST", url, &soapRequest)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	// req.Header.Add("SOAPAction", soapAction)
	req.Header.Set("User-Agent", "gitlab.com/commondatafactory/")
	req.Close = true

	// We expect a response within 5 seconds.
	client := &http.Client{
		Timeout: 15 * time.Second,
	}

	// Save a copy of this request for debugging.
	requestDump, err := httputil.DumpRequest(req, true)

	if err != nil {
		fmt.Println(err)
	}

	if settingo.GetBool("debug") {
		fmt.Println(string(requestDump))
	}

	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if settingo.GetBool("debug") {
		_b, _ := httputil.DumpResponse(resp, true)
		fmt.Println(string(_b))
	}

	if resp.StatusCode != http.StatusOK {
		// read only the first MiB of the body in error case
		limReader := io.LimitReader(resp.Body, 1024*1024)
		body, _ := ioutil.ReadAll(limReader)

		_b, _ := httputil.DumpResponse(resp, true)
		fmt.Println(string(_b))
		return nil, &HTTPError{
			StatusCode: resp.StatusCode,
			Status:     resp.Status,
			Msg:        string(body),
		}
	}

	if err != nil {
		return nil, err
	}

	rawbody, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	if len(rawbody) == 0 {
		return nil, errors.New("Empty response")
	}

	envelope, _ := SoapFomMTOM(rawbody)
	err = CheckFault(envelope)

	if err != nil {
		return nil, err
	}

	return envelope, nil
}

func SoapFomMTOM(soap []byte) ([]byte, error) {
	reg := regexp.MustCompile(`(?ims)<[env:|soap:].+Envelope>`)
	s := reg.FindString(string(soap))
	if s == "" {
		return nil, errors.New("Response without soap envelope")
	}

	return []byte(s), nil
}

// TODO fix fault responses for dutch api. exeptie / fout
func CheckFault(soapResponse []byte) error {
	xmlEnvelope := ResponseEnvelope{}

	err := xml.Unmarshal(soapResponse, &xmlEnvelope)

	if err != nil {
		return err
	}

	fault := xmlEnvelope.ResponseBodyBody.Fault
	if fault.XMLName.Local == "Fault" {
		sFault := fault.Code + " | " + fault.String + " | " + fault.Actor + " | " + fault.Detail
		return errors.New(sFault)
	}

	return nil
}
