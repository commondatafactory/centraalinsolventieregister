package soap

import (
	"encoding/hex"
	"fmt"
	"math/rand"

	"github.com/Attumm/settingo/settingo"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

const anonymous = "http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous"

func randomHex(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

func getMessageID() string {
	hex8, _ := randomHex(4)
	hex4, _ := randomHex(2)
	msg := fmt.Sprintf("urn:uuid:%s-%s", hex8, hex4)
	return msg
}

func RandStringBytesMask(n int) string {
	b := make([]byte, n)
	for i := 0; i < n; {
		idx := rand.Intn(len(letterBytes))
		b[i] = letterBytes[idx]
		i++
	}

	return string(b)
}

func MakeAuthEnvelope() *Envelope {
	env := &Envelope{
		EnvelopeAttr: "http://schemas.xmlsoap.org/soap/envelope/",
		EncodeAttr:   "http://www.w3.org/2003/05/soap-encoding",
		WSAAttr:      "http://schemas.xmlsoap.org/ws/2004/08/addressing",
		NSAttr:       "http://www.rechtspraak.nl/namespaces/cir01",
		XSIAttr:      "http://www.w3.org/2001/XMLSchema-instance",
		Header: &Header{
			WsseSecurity: &WsseSecurity{
				XmlnsWsse: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
				XmlnsWsu:  "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
				// MustUnderstand: "1",
				UsernameToken: &UsernameToken{
					Username: &Username{},
					Password: &Password{},
					Nonce:    &Nonce{},
				},
			},
			ReplyTo: &ReplyTo{
				Address: &Address{
					Value: anonymous,
				},
			},
			To: &To{
				Value: "http://webservice.rechtspraak.nl/cir.asmx",
			},
			MessageID: &MessageID{
				Value: getMessageID(),
			},
			Action: &Action{
				Value: "http://www.rechtspraak.nl/namespaces/cir01/GetLastUpdate",
			},
		},
		Body: &Body{
			Payload: &GetLastUpdate{},
		},
	}

	if settingo.Get("SOAPPASSWORD") == "" {
		panic("missing soap password")
	}

	env.Header.WsseSecurity.UsernameToken.Username.Value = settingo.Get("SOAPUSER")
	env.Header.WsseSecurity.UsernameToken.Password.Value = settingo.Get("SOAPPASSWORD")
	env.Header.WsseSecurity.UsernameToken.Nonce.Value = RandStringBytesMask(64)

	return env
}
