package soap

import (
	"bytes"
	"encoding/xml"
	"log"
)

// Envelope is a SOAP envelope.
type Envelope struct {
	XMLName      xml.Name `xml:"SOAP-ENV:Envelope"`
	EnvelopeAttr string   `xml:"xmlns:SOAP-ENV,attr"`
	EncodeAttr   string   `xml:"xmlns:SOAP-ENC,attr,omitempty"`
	NSAttr       string   `xml:"xmlns:ns,attr"`
	WSAAttr      string   `xml:"xmlns:wsa,attr,omitempty"`
	TNSAttr      string   `xml:"xmlns:tns,attr,omitempty"`
	URNAttr      string   `xml:"xmlns:urn,attr,omitempty"`
	XSIAttr      string   `xml:"xmlns:xsi,attr,omitempty"`
	Header       *Header  `xml:"SOAP-ENV:Header"`
	Body         *Body    `xml:"SOAP-ENV:Body"`
}

func (e *Envelope) SetAction(action string) {
	e.Header.Action.Value = "http://www.rechtspraak.nl/namespaces/cir01/" + action
}

func (e *Envelope) SetBody(body *Body) {
	e.Body = body
}

func (e *Envelope) GetBuffer() bytes.Buffer {
	var b bytes.Buffer
	b.Write([]byte(xml.Header))
	err := xml.NewEncoder(&b).Encode(e)
	if err != nil {
		log.Printf("building soap message failed")
		panic(err)
	}
	return b
}

type Body struct {
	XMLName xml.Name `xml:"SOAP-ENV:Body"`
	Payload interface{}
}

type Header struct {
	XMLName      xml.Name `xml:"SOAP-ENV:Header"`
	WsseSecurity *WsseSecurity
	ReplyTo      *ReplyTo
	To           *To
	Address      *Address
	MessageID    *MessageID
	Action       *Action
}

type WsseSecurity struct {
	// MustUnderstand string   `xml:"SOAP-ENV:mustUnderstand,attr"`
	XMLName   xml.Name `xml:"wsse:Security"`
	XmlnsWsse string   `xml:"xmlns:wsse,attr"`
	XmlnsWsu  string   `xml:"xmlns:wsu,attr"`

	UsernameToken *UsernameToken
}

type UsernameToken struct {
	XMLName  xml.Name `xml:"wsse:UsernameToken"`
	WsuId    string   `xml:"wsu:Id,attr,omitempty"`
	Username *Username
	Password *Password
	Nonce    *Nonce
	Created  *Created
}

type Username struct {
	XMLName xml.Name `xml:"wsse:Username"`
	Value   string   `xml:",chardata"`
}
type Password struct {
	XMLName xml.Name `xml:"wsse:Password"`
	Type    string   `xml:"Type,attr"`
	Value   string   `xml:",chardata"`
}
type Nonce struct {
	XMLName      xml.Name `xml:"wsse:Nonce,omitempty"`
	EncodingType string   `xml:"EncodingType,attr,omitempty"`
	Value        string   `xml:",chardata"`
}
type Created struct {
	XMLName xml.Name `xml:"wsu:Created,omitempty"`
	Value   string   `xml:",chardata"`
}

type ReplyTo struct {
	XMLName xml.Name `xml:"wsa:ReplyTo,omitempty"`
	Address *Address
}

type To struct {
	XMLName xml.Name `xml:"wsa:To,omitempty"`
	Value   string   `xml:",chardata"`
}

type MessageID struct {
	XMLName xml.Name `xml:"wsa:MessageID,omitempty"`
	Value   string   `xml:",chardata"`
}

type Address struct {
	XMLName xml.Name `xml:"wsa:Address,omitempty"`
	Value   string   `xml:",chardata"`
}

type Action struct {
	XMLName xml.Name `xml:"wsa:Action,omitempty"`
	Value   string   `xml:",chardata"`
}

type GetLastUpdate struct {
	XMLName xml.Name `xml:"ns:GetLastUpdate"`
}

/*
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <searchReportsSince xmlns="http://www.rechtspraak.nl/namespaces/cir01">
      <datetimeFrom>dateTime</datetimeFrom>
      <datetimeTo>dateTime</datetimeTo>
    </searchReportsSince>
  </soap:Body>
</soap:Envelope>
*/

type DateTimeFrom struct {
	XMLName xml.Name `xml:"ns:datetimeFrom"`
	Value   string   `xml:",chardata"`
}

type DateTimeTo struct {
	XMLName xml.Name `xml:"ns:datetimeTo"`
	Value   string   `xml:",chardata"`
}

type SearchReportsSince struct {
	XMLName      xml.Name `xml:"ns:searchReportsSince"`
	DateTimeFrom *DateTimeFrom
	DateTimeTo   *DateTimeTo
}

/*
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getCaseWithReports xmlns="http://www.rechtspraak.nl/namespaces/cir01">
      <publicationNumber>string</publicationNumber>
    </getCaseWithReports>
  </soap:Body>
</soap:Envelope>
*/

type PublicationNumber struct {
	XMLName xml.Name `xml:"ns:publicationNumber"`
	Value   string   `xml:",chardata"`
}

type GetCaseWithReportsSoapIn struct {
	XMLName           xml.Name `xml:"ns:getCaseWithReports"`
	PublicationNumber *PublicationNumber
}

/*
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <searchInsolvencyID xmlns="http://www.rechtspraak.nl/namespaces/cir01">
      <insolvencyID>string</insolvencyID>
      <court>string</court>
    </searchInsolvencyID>
  </soap:Body>
</soap:Envelope>
*/

type InsolvencyID struct {
	XMLName xml.Name `xml:"ns:insolvencyID"`
	Value   string   `xml:",chardata"`
}

type CourtCode struct {
	XMLName xml.Name `xml:"ns:court"`
	Value   string   `xml:",chardata"`
}

type SearchInsolvencyIDsoapIn struct {
	XMLName    xml.Name `xml:"ns:searchInsolvencyID"`
	Insolvency *InsolvencyID
	Court      *CourtCode
}
