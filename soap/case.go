// Copytright 2020 Stephan Preeker. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package soap

import (
	"encoding/xml"
	"strconv"
)

type InsolvencyNumber struct {
	Value string `xml:",chardata"`
}

type Code struct {
	Value string `xml:",chardata"`
}

func (c *Code) Number() int {
	number, _ := strconv.Atoi(c.Value)
	return number
}

type Name struct {
	Value string `xml:",chardata"`
}

type Person struct {
	Rechtspersoon  *Name `xml:"rechtspersoonlijkheid"`
	FirstName      *Name `xml:"voornaam,omitempty" json:",omitempty" `
	Initials       *Name `xml:"voorletters,omitempty" json:",omitempty"`
	MiddlePartName *Name `xml:"voorvoegsel,omitempty" json:",omitempty"`
	Name           *Name `xml:"achternaam"` // Name // Trading name
	KvKNumber      *Code `xml:"KvKNummer,omitempty" json:",omitempty"`
	KvKTown        *Name `xml:"KvKPlaats,omitempty" json:",omitempty"`
	Country        *Name `xml:"LandNaam,omitempty" json:",omitempty"`
	BirthPlace     *Name `xml:"Geboorteplaats,omitempty" json:",omitempty"`
	BirthDate      *Name `xml:"Geboortedatum,omitempty" json:",omitempty"`
	BirthCountry   *Name `xml:"Geboorteland,omitempty" json:",omitempty"`
	DeathDate      *Name `xml:"Overleidensdatum" json:",omitempty"`
}

type Adres struct {
	DateStart    *Name `xml:"datumBegin" json:",omitempty"`
	Street       *Name `xml:"straat" json:",omitempty"`
	StreetNumber *Code `xml:"huisnummer" json:",omitempty"`
	Addition1    *Name `xml:"huisnummerToevoeging1" json:",omitempty"`
	Addition2    *Name `xml:"huisnummerToevoeging2" json:",omitempty"`
	PostalCode   *Code `xml:"postcode" json:",omitempty"`
	Town         *Name `xml:"plaats" json:",omitempty"`
	Type         *Name `xml:"adresType" json:",omitempty"`
	Phone        *Code `xml:"telefoonnummer,omitempty" json:",omitempty"`
	Country      *Code `xml:",omitempty" json:",omitempty"`
}

type Adresses struct {
	AdresList []Adres `xml:"adres"`
}

type TradeAdresses struct {
	AdresList []Adres `xml:"handelsadres"`
}

type TradingName struct {
	Name      *Name          `xml:"handelsnaam"`
	KvKNumber *Code          `xml:"KvKNummer"`
	Locations *TradeAdresses `xml:"handelsadressen"`
}

type TradingNames struct {
	TradingBy []*TradingName `xml:"handelendOnderDeNaam"`
}

type Administrator struct { //Curator / Bewindvoerder.
	DateStart  *Name  `xml:"datumBegin"`
	Initials   *Name  `xml:"voorletters"`
	FamilyName *Name  `xml:"achternaam"`
	Type       *Code  `xml:"CB"` // C curator B bewindvoerder
	Adres      *Adres `xml:"adres"`
}

type Cbvers struct {
	Curator *Administrator `xml:"cbv"`
}

type Publication struct {
	Date            *Date  `xml:"publicatieDatum"`
	Code            *Code  `xml:"publicatieKenmerk"`
	Description     *Name  `xml:"publicatieOmschrijving"`
	CodeType        *Code  `xml:"publicatieSoortCode"`
	HearingLocation *Adres `xml:"zittingslocatie"`
	CourtCaseCode   *Code  `xml:"publicerendeInstantieCode"`
}

func (p *Publication) CodeDescription() string {
	return CaseCodes[p.CodeType.Value]
}

type PublicationHistory struct {
	Publications []*Publication `xml:"publicatie"`
}

type CaseReport struct {
	Date  *Date `xml:"publicatiedatumVerslag"`
	Code  *Code `xml:"kenmerk"`
	Title *Name `xml:"titel"`
	Final *Code `xml:"eindverslag"`
}

type Reports struct {
	Report *[]CaseReport `xml:"verslag"`
}

type Insolvency struct {
	InsolventieNumber         InsolvencyNumber    `xml:"insolventienummer"`
	CourtCode                 *Code               `xml:"behandelendeInstantieCode"`
	CourtName                 *Name               `xml:"behandelendeInstantieNaam"`
	CourtLocationCode         *Code               `xml:"behandelendeVestigingCode"`
	CourtLocationName         *Code               `xml:"behandelendeVestigingNaam"`
	Person                    *Person             `xml:"persoon"`
	Addresses                 *Adresses           `xml:"adressen"`
	TradingNames              *TradingNames       `xml:"handelendOnderDeNamen"`
	Administrator             *Cbvers             `xml:"cbvers"`
	SupervisoryJudge          *Name               `xml:"RC" json:",omitempty"` // rechter commissaris
	PrevisousSupervisoryJudge *Name               `xml:"vorigeRC" json:",omitempty"`
	Publications              *PublicationHistory `xml:"publicatiegeschiedenis"`
	AvailableReports          *Reports            `xml:"beschikbareVerslagen"` //verslagen
	EndDate                   *Date               `xml:"eindeVindbaarheid" json:",omitempty"`
}

type WebserviceIns struct {
	Insolvency *Insolvency `xml:"insolvente"`
}

type CaseResult struct {
	WebserviceIns *WebserviceIns `xml:"inspubWebserviceInsolvente"`
}

type CompleteCaseReport struct {
	CaseResult *CaseResult `xml:"getCaseWithReportsResult"`
}

type OperationGetCaseReport struct {
	CaseReport *CompleteCaseReport `xml:"getCaseWithReportsResponse"`
}

func (o *OperationGetCaseReport) ParseXML(input []byte) error {

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    *OperationGetCaseReport
	}{Body: o}

	err := xml.Unmarshal(input, &marshalStructure)
	return err

}

func (o *OperationGetCaseReport) Insolvency() *Insolvency {
	return o.CaseReport.CaseResult.WebserviceIns.Insolvency
}

func (o *OperationGetCaseReport) Publications() []*Publication {
	if o.Insolvency() != nil && o.Insolvency().Publications != nil && o.Insolvency().Publications.Publications != nil {
		return o.Insolvency().Publications.Publications
	}
	return []*Publication{}
}

type InsolvencyLocation struct {
	TradingName *TradingName
	Person      *Person
	Adres       *Adres
}

// AllAddresses extract all relevant address for locations with Trade or Person information.
func (o *OperationGetCaseReport) AllAddresses() []*InsolvencyLocation {

	ins := o.Insolvency()
	out := []*InsolvencyLocation{}

	if ins == nil {
		return out
	}

	if ins.Addresses != nil && ins.Addresses.AdresList != nil {
		for _, a := range ins.Addresses.AdresList {
			nl := InsolvencyLocation{
				Person: ins.Person,
				Adres:  &a,
			}
			out = append(out, &nl)
		}
	}

	if ins.TradingNames == nil {
		return out
	}

	if ins.TradingNames.TradingBy == nil {
		return out
	}

	// extract all adresses related to trading names.
	for _, tn := range ins.TradingNames.TradingBy {
		if tn.Locations == nil {
			continue
		}
		if tn.Locations.AdresList == nil {
			continue
		}
		for _, a := range tn.Locations.AdresList {
			nl := InsolvencyLocation{
				TradingName: tn,
				Adres:       &a,
			}
			out = append(out, &nl)
		}
	}

	return out
}
