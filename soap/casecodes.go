package soap

import (
	"fmt"
)

type caseCodes map[string]string

var CaseCodes caseCodes

func init() {

	CaseCodes = caseCodes{}

	loadCaseCodesFromXSD()
}

func loadCaseCodesFromXSD() {

	source := "../docs/RSpublicWS_InsolvencyContent02.xsd"
	schema, err := loadSchemaFile(source)

	if err != nil {
		panic(err)
	}

	for i := range schema.SimpleTypes {
		if schema.SimpleTypes[i].Name == "inspubPublicatiesoortCode" {

			if schema.SimpleTypes[i].Restriction == nil {
				continue
			}

			enums := schema.SimpleTypes[i].Restriction.Enumerations

			for ii := range enums {
				e := enums[ii]
				Description := enums[ii].Annotation[0].Documentations[1].Value
				CaseCodes[e.Code] = Description
			}
		}
	}

	fmt.Printf("Found case codes %d \n", len(CaseCodes))

	if len(CaseCodes) == 0 {
		panic("missing case codes")
	}
}
