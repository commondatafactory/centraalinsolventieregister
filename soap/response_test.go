package soap

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func ExampleResponse() {
	s := `
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
   <env:Body>
      <env:Fault>
         <faultcode>env:Client</faultcode>
         <faultstring>Rejected (from client)</faultstring>
      </env:Fault>
   </env:Body>
</env:Envelope>`

	soapTest := []byte(s)

	env := ResponseEnvelope{}

	xml.Unmarshal(soapTest, &env)

	json, _ := json.Marshal(env)
	os.Stdout.Write(json)

	// Output:
	// {"XMLName":{"Space":"http://schemas.xmlsoap.org/soap/envelope/","Local":"Envelope"},"ResponseBodyBody":{"XMLName":{"Space":"http://schemas.xmlsoap.org/soap/envelope/","Local":"Body"},"Fault":{"XMLName":{"Space":"http://schemas.xmlsoap.org/soap/envelope/","Local":"Fault"},"Code":"env:Client","String":"Rejected (from client)","Actor":"","Detail":""}}}

}

func TestResponseFault(t *testing.T) {
	s := `
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
   <env:Body>
      <env:Fault>
         <faultcode>env:Client</faultcode>
         <faultstring>Rejected (from client)</faultstring>
      </env:Fault>
   </env:Body>
</env:Envelope>`

	soapTest := []byte(s)

	err := CheckFault(soapTest)
	if err == nil {
		t.Error("Expected fault")
	}

}

func TestResponseNotFault(t *testing.T) {
	s := `
<soap:Envelope
	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:GetMetadataInstanceAllValueResponse
	xmlns:ns2="http://www.imgw.pl/c26/ws/metadata/universalmetadataservice"><value
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:ns4="http://www.imgw.pl/c26/ws/metadata/metadatatypes"
	xsi:type="ns4:MetadataCollectionType"
	partialId="station[349190600]"><collection partialId=".core"><values
	partialId=".nazwa"><value
	xmlns:ns5="http://www.imgw.pl/c26/ws/metadata/coretypes"
	xsi:type="ns5:NameStationValueType" value="BIELSKO-BIAĹA"
	validFrom="1999-06-30T00:00:00.000Z"/></values></collection></value>
	</ns2:GetMetadataInstanceAllValueResponse></soap:Body>
</soap:Envelope>`
	soapTest := []byte(s)

	err := CheckFault(soapTest)
	if err != nil {
		t.Error("Expected positive")
	}

}

func ExamplePublicationList() {
	s := `
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
<searchInsolvencyIDResponse xmlns="http://www.rechtspraak.nl/namespaces/cir01">
<searchInsolvencyIDResult>
<publicatieLijst xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.rechtspraak.nl/namespaces/inspubber01" extractiedatum="2021-06-07T15:15:58.8675005+02:00">
	<publicatieKenmerk>01.dha.11.7788.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.obr.13.1227.F.1308.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.obr.14.620.F.1316.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.obr.15.711.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.1060.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.1395.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.1596.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.1700.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.2364.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.2394.F.1300.1.17</publicatieKenmerk>
	<publicatieKenmerk>01.gel.16.2819.F.1300.1.17</publicatieKenmerk>
</publicatieLijst>
</searchInsolvencyIDResult>
</searchInsolvencyIDResponse>
</env:Envelope>
`
	p := []byte(s)

	//out := SearchInsolvencyIDResult{}
	//out := SearchInsolvencyResponse{}
	out := OperationSearchInsolvencyIDResponseSoapOut{}

	err := xml.Unmarshal(p, &out)
	if err != nil {
		fmt.Println(err)
		return
	}

	json, _ := json.Marshal(out)
	os.Stdout.Write(json)
	// Output:
	// {"Response":{"Result":{"publicatieLijst":{"publicatieKenmerk":[{"Value":"01.dha.11.7788.F.1300.1.17"},{"Value":"01.obr.13.1227.F.1308.1.17"},{"Value":"01.obr.14.620.F.1316.1.17"},{"Value":"01.obr.15.711.F.1300.1.17"},{"Value":"01.gel.16.1060.F.1300.1.17"},{"Value":"01.gel.16.1395.F.1300.1.17"},{"Value":"01.gel.16.1596.F.1300.1.17"},{"Value":"01.gel.16.1700.F.1300.1.17"},{"Value":"01.gel.16.2364.F.1300.1.17"},{"Value":"01.gel.16.2394.F.1300.1.17"},{"Value":"01.gel.16.2819.F.1300.1.17"}],"extractiedatum":"2021-06-07T15:15:58.8675005+02:00"}}}}

}

func ExampleGetLastUpdateResponse() {
	file, _ := os.Open("testdata/getlastupdate.xml")
	response, _ := ioutil.ReadAll(file)

	enveloppe, _ := SoapFomMTOM(response)
	_ = CheckFault(enveloppe)

	out := OperationGetLastUpdateSoapOut{}

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    *OperationGetLastUpdateSoapOut
	}{Body: &out}

	_ = xml.Unmarshal(response, &marshalStructure)

	json, _ := json.Marshal(marshalStructure)
	os.Stdout.Write(json)

	os.Stdout.Write([]byte("\n"))
	os.Stdout.Write([]byte(out.Result()))
	// Output:
	// {"XMLName":{"Space":"http://schemas.xmlsoap.org/soap/envelope/","Local":"Envelope"},"Body":{"GetLastUpdateResponse":{"GetLastUpdateResult":{"LastUpdate":{"LastUpdateDate":{"Value":"2021-06-02"}}}}}}
	// 2021-06-02
}

func ExampleSearchReportsSince() {
	file, err := os.Open("testdata/updatesince.xml")
	if err != nil {
		fmt.Println(err)
		return
	}
	response, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return
	}

	enveloppe, _ := SoapFomMTOM(response)
	_ = CheckFault(enveloppe)

	out := OperationSearchReportsSinceSoapOut{}

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    *OperationSearchReportsSinceSoapOut
	}{Body: &out}

	err = xml.Unmarshal(response, &marshalStructure)
	if err != nil {
		fmt.Println(err)
		return
	}

	reports := out.Reports()
	json, _ := json.Marshal(reports[0])

	fmt.Println(len(reports))
	fmt.Println(string(json))
	fmt.Println(reports[0].Publicatie.Parse())
	// Output:
	// 155
	// {"Insolventienummer":{"Value":"F.08/19/274"},"Rechtbank":{"Value":"Overijssel"},"Publicatie":{"Value":"2021-06-03T16:00:13"},"Kenmerk":{"Value":"08_ove_19_274_F_V_07_B"},"Titel":{"Value":"Financieel verslag 15-12-2020"},"Eindverslag":{"Value":"false"}}
	// 2021-06-03 16:00:13 +0200 CEST
}

func ExampleSearchInsolvencyResponse() {
	s := `
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
<searchInsolvencyIDResponse xmlns="http://www.rechtspraak.nl/namespaces/cir01">
  <searchInsolvencyIDResult>
     <publicatieLijst xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.rechtspraak.nl/namespaces/inspubber01" extractiedatum="2021-06-07T15:15:58.8675005+02:00">
        <publicatieKenmerk>01.obr.20.183.F.1325.1.21</publicatieKenmerk>
        <publicatieKenmerk>01.obr.20.183.F.1325.1.22</publicatieKenmerk>
     </publicatieLijst>
  </searchInsolvencyIDResult>
</searchInsolvencyIDResponse>
</env:Envelope>
`
	response := []byte(s)

	marshalStructure := OperationSearchInsolvencyIDResponseSoapOut{}

	err := xml.Unmarshal(response, &marshalStructure)

	if err != nil {
		panic(err)
	}

	ojson, _ := json.Marshal(marshalStructure)
	os.Stdout.Write(ojson)

	fmt.Println()
	fmt.Println(marshalStructure.GetPublicationList()[0].Value)

	// Output:
	// {"Response":{"Result":{"publicatieLijst":{"publicatieKenmerk":[{"Value":"01.obr.20.183.F.1325.1.21"},{"Value":"01.obr.20.183.F.1325.1.22"}],"extractiedatum":"2021-06-07T15:15:58.8675005+02:00"}}}}
	// 01.obr.20.183.F.1325.1.21
}
