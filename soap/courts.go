package soap

import (
	"fmt"
	"strings"
)

type courtRegister map[string]string

var CourtIDs courtRegister
var CourtNames courtRegister

func init() {
	CourtIDs = courtRegister{}
	CourtNames = courtRegister{}

	// Load provided XSD in documentation to find all valid court ID's
	loadCourtsFromXSD()
}

/*
<xs:simpleType name="instantieRechtbankCodeHGK">
	<xs:restriction base="xs:string">
	<xs:enumeration value="40">
		<xs:annotation>
			<xs:documentation xml:lang="nl">Amsterdam</xs:documentation>
			<xs:documentation xml:lang="en">Amsterdam</xs:documentation>
		</xs:annotation>
...
<xs:simpleType name="instantieRechtbankCodePreHGK">
		<xs:restriction base="xs:string">
			<xs:enumeration value="01">
				<xs:annotation>
					<xs:documentation xml:lang="nl">'s-Hertogenbosch</xs:documentation>
					<xs:documentation xml:lang="en">'s-Hertogenbosch</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
</xs:simpleType>
...
*/

func loadCourtsFromXSD() {

	schema, err := loadSchemaFile("../docs/RSpublicWS_InsolvencyRequests.xsd")

	if err != nil {
		fmt.Println(err)
		panic("loading courtcodes failed")
	}

	for i := range schema.SimpleTypes {
		if strings.HasPrefix(schema.SimpleTypes[i].Name, "instantieRechtbankCode") {
			if schema.SimpleTypes[i].Restriction == nil {
				continue
			}

			enums := schema.SimpleTypes[i].Restriction.Enumerations

			for ii := range enums {
				e := enums[ii]
				courtName := enums[ii].Annotation[0].Documentations[0].Value
				CourtIDs[courtName] = e.Code
				CourtNames[e.Code] = courtName
			}
		}
	}
	fmt.Printf("Found courts %d \n", len(CourtIDs))
}
