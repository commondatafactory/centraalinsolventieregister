package soap

import (
	"encoding/xml"
	"fmt"
	"log"
	"time"
)

var url string

func init() {
	url = "https://webservice.rechtspraak.nl/cir.asmx"
}

func GetLastupdate() (time.Time, error) {
	envelope := MakeAuthEnvelope()

	b := envelope.GetBuffer()
	response, err := SoapRequest(url, b)

	if err != nil {
		log.Printf("soap request failed")
		return time.Time{}, err
	}

	out := OperationGetLastUpdateSoapOut{}

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    OperationGetLastUpdateSoapOut
	}{Body: out}

	_ = xml.Unmarshal(response, &marshalStructure)

	if err != nil {
		log.Printf("xml parsing failed")
		return time.Time{}, err
	}

	date := marshalStructure.Body.Result()
	lastUpdate, _ := time.Parse("2006-01-02", date)
	return lastUpdate, nil
}

func GetLastupdateSince(from time.Time, to time.Time) []Report {
	envelope := MakeAuthEnvelope()
	envelope.SetAction("searchReportsSince")
	body := &Body{
		Payload: &SearchReportsSince{
			DateTimeFrom: &DateTimeFrom{
				Value: from.Format("2006-01-02T15:04:05"),
			},
			DateTimeTo: &DateTimeTo{
				Value: to.Format("2006-01-02T15:04:05"),
			},
		},
	}

	envelope.SetBody(body)

	b := envelope.GetBuffer()

	response, err := SoapRequest(url, b)

	if err != nil {
		log.Printf("soap request failed")
		log.Print(err)
		return []Report{}
	}

	out := OperationSearchReportsSinceSoapOut{}

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    *OperationSearchReportsSinceSoapOut
	}{Body: &out}

	err = xml.Unmarshal(response, &marshalStructure)
	if err != nil {
		fmt.Println(err)
		return []Report{}
	}

	reports := out.Reports()

	// debug
	//if len(reports) > 0 {
	//	json, _ := json.Marshal(reports[0])
	//	os.Stdout.Write(json)
	//}

	// fmt.Println(len(reports))

	return reports
}

func SearchInsolvencyID(insolvencyID string, court string) []*PublicationCode {

	envelope := MakeAuthEnvelope()
	envelope.SetAction("getCaseWithReportsResult")

	body := &Body{
		Payload: &SearchInsolvencyIDsoapIn{
			Insolvency: &InsolvencyID{
				Value: insolvencyID,
			},
			Court: &CourtCode{
				Value: court,
			},
		},
	}

	envelope.SetBody(body)
	b := envelope.GetBuffer()
	reponse, err := SoapRequest(url, b)

	if err != nil {
		log.Printf("soap request failed")
		return []*PublicationCode{}
	}

	out := OperationSearchInsolvencyIDResponseSoapOut{}

	marshalStructure := struct {
		XMLName xml.Name `xml:"Envelope"`
		Body    *OperationSearchInsolvencyIDResponseSoapOut
	}{Body: &out}

	err = xml.Unmarshal(reponse, &marshalStructure)
	if err != nil {
		fmt.Println(err)
		return []*PublicationCode{}
	}

	// json, _ := json.Marshal(out)
	// os.Stdout.Write(json)
	return out.GetPublicationList()
}

// GetCaseWithReport return an actual insolvency case for given publication number.
// returns both the parsed version and the string buffer.
// the string version can be used with cacheing.
func GetCaseWithReports(publicationnumber string) (OperationGetCaseReport, string, error) {

	envelope := MakeAuthEnvelope()
	envelope.SetAction("getCaseWithReportsResult")

	body := &Body{
		Payload: &GetCaseWithReportsSoapIn{
			PublicationNumber: &PublicationNumber{
				Value: publicationnumber,
			},
		},
	}

	envelope.SetBody(body)
	b := envelope.GetBuffer()
	response, err := SoapRequest(url, b)

	out := OperationGetCaseReport{}

	if err != nil {
		log.Printf("soap request failed")
		return out, "", err
	}

	err = out.ParseXML(response)
	if err != nil {
		fmt.Println(err)
		return out, "", err
	}

	// json, _ := json.MarshalIndent(out, "", "  ")
	// os.Stdout.Write(json)
	return out, string(response), nil
}
