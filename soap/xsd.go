package soap

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"

	"path"
)

type Documentation struct {
	XMLName xml.Name `xml:"documentation"`
	Lang    string   `xml:"lang,attr"`
	Value   string   `xml:",chardata"`
}

type Annotation struct {
	XMLName        xml.Name        `xml:"annotation"`
	Documentations []Documentation `xml:"documentation"`
}

type Enumeration struct {
	XMLName    xml.Name     `xml:"enumeration"`
	Code       string       `xml:"value,attr"`
	Annotation []Annotation `xml:"annotation"`
}

type Restriction struct {
	XMLName      xml.Name      `xml:"restriction"`
	Enumerations []Enumeration `xml:"enumeration"`
}

type SimpleType struct {
	XMLName     xml.Name     `xml:"simpleType"`
	Name        string       `xml:"name,attr"`
	Restriction *Restriction `xml:"restriction"`
}

type Schema struct {
	XMLName     xml.Name     `xml:"schema"`
	XSattr      string       `xml:"elementFormDefault,attr"`
	SimpleTypes []SimpleType `xml:"simpleType"`
}

func loadSchemaFile(xsdpath string) (Schema, error) {

	_, filename, _, _ := runtime.Caller(1)
	source := path.Join(path.Dir(filename), xsdpath)

	var schema Schema
	file, err := os.Open(source)
	if err != nil {
		fmt.Println(err)
		return schema, err
	}
	defer file.Close()

	xsd, err := ioutil.ReadAll(file)

	if err != nil {
		fmt.Println(err)
		return schema, err
	}

	err = xml.Unmarshal(xsd, &schema)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	return schema, nil
}
