package soap

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
)

func readTestCase() OperationGetCaseReport {

	out := OperationGetCaseReport{}
	file, err := os.Open("testdata/case.xml")
	if err != nil {
		fmt.Println(err)
		return out
	}
	response, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return out
	}

	enveloppe, _ := SoapFomMTOM(response)
	_ = CheckFault(enveloppe)

	err = out.ParseXML(response)

	if err != nil {
		fmt.Println(err)
	}

	return out
}

func ExampleCaseReport() {
	out := readTestCase()
	json, _ := json.MarshalIndent(out, "", "  ")
	os.Stdout.Write(json)

	// Output:

	// {
	//   "CaseReport": {
	//     "CaseResult": {
	//       "WebserviceIns": {
	//         "Insolvency": {
	//           "InsolventieNumber": {
	//             "Value": "F.09/19/183"
	//           },
	//           "CourtCode": {
	//             "Value": "48"
	//           },
	//           "CourtName": {
	//             "Value": "Rechtbank Oost-Brabant"
	//           },
	//           "CourtLocationCode": {
	//             "Value": "3304"
	//           },
	//           "CourtLocationName": {
	//             "Value": "'s-Hertogenbosch"
	//           },
	//           "Person": {
	//             "Rechtspersoon": {
	//               "Value": "rechtspersoon"
	//             },
	//             "Name": {
	//               "Value": "XXX Locatiebeheer B.V."
	//             },
	//             "KvKNumber": {
	//               "Value": "56933395"
	//             }
	//           },
	//           "Addresses": {
	//             "AdresList": [
	//               {
	//                 "DateStart": {
	//                   "Value": "2016-05-24"
	//                 },
	//                 "Street": {
	//                   "Value": "Marathonloop"
	//                 },
	//                 "StreetNumber": {
	//                   "Value": "6"
	//                 },
	//                 "PostalCode": {
	//                   "Value": "5235AA"
	//                 },
	//                 "Town": {
	//                   "Value": "'s-Hertogenbosch"
	//                 },
	//                 "Type": {
	//                   "Value": "VEST"
	//                 }
	//               },
	//               {
	//                 "DateStart": {
	//                   "Value": "2016-05-24"
	//                 },
	//                 "Street": {
	//                   "Value": "Marathonloop"
	//                 },
	//                 "StreetNumber": {
	//                   "Value": "5"
	//                 },
	//                 "PostalCode": {
	//                   "Value": "5235AA"
	//                 },
	//                 "Town": {
	//                   "Value": "'s-Hertogenbosch"
	//                 },
	//                 "Type": {
	//                   "Value": "VEST"
	//                 }
	//               }
	//             ]
	//           },
	//           "TradingNames": {
	//             "TradingBy": [
	//               {
	//                 "Name": {
	//                   "Value": "XXX Locatiebeheer B.V."
	//                 },
	//                 "KvKNumber": {
	//                   "Value": "56932295"
	//                 },
	//                 "Locations": {
	//                   "AdresList": [
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-05-22"
	//                       },
	//                       "Street": {
	//                         "Value": "Oosteinderweg"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "50"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5247WG"
	//                       },
	//                       "Town": {
	//                         "Value": "Rosmalen"
	//                       },
	//                       "Type": {
	//                         "Value": "CORR"
	//                       }
	//                     },
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-04-24"
	//                       },
	//                       "Street": {
	//                         "Value": "Marathonloop"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "2"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5235AA"
	//                       },
	//                       "Town": {
	//                         "Value": "'s-Hertogenbosch"
	//                       },
	//                       "Type": {
	//                         "Value": "VEST"
	//                       }
	//                     }
	//                   ]
	//                 }
	//               },
	//               {
	//                 "Name": {
	//                   "Value": "Blabla Maaspoort"
	//                 },
	//                 "KvKNumber": {
	//                   "Value": "34441385"
	//                 },
	//                 "Locations": {
	//                   "AdresList": [
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-05-22"
	//                       },
	//                       "Street": {
	//                         "Value": "Oosteinderweg"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "52"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5247WG"
	//                       },
	//                       "Town": {
	//                         "Value": "Rosmalen"
	//                       },
	//                       "Type": {
	//                         "Value": "CORR"
	//                       }
	//                     },
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-04-24"
	//                       },
	//                       "Street": {
	//                         "Value": "Marathonloop"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "7"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5235AA"
	//                       },
	//                       "Town": {
	//                         "Value": "'s-Hertogenbosch"
	//                       },
	//                       "Type": {
	//                         "Value": "VEST"
	//                       }
	//                     },
	//                     {
	//                       "DateStart": {
	//                         "Value": "2017-09-28"
	//                       },
	//                       "Street": {
	//                         "Value": "Marathonloop"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "1"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5235AA"
	//                       },
	//                       "Town": {
	//                         "Value": "'s-Hertogenbosch"
	//                       },
	//                       "Type": {
	//                         "Value": "VEST"
	//                       }
	//                     }
	//                   ]
	//                 }
	//               },
	//               {
	//                 "Name": {
	//                   "Value": "lalala eten"
	//                 },
	//                 "KvKNumber": {
	//                   "Value": "56933395"
	//                 },
	//                 "Locations": {
	//                   "AdresList": [
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-05-22"
	//                       },
	//                       "Street": {
	//                         "Value": "Oosteinderweg"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "52"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5247WG"
	//                       },
	//                       "Town": {
	//                         "Value": "Rosmalen"
	//                       },
	//                       "Type": {
	//                         "Value": "CORR"
	//                       }
	//                     },
	//                     {
	//                       "DateStart": {
	//                         "Value": "2019-04-24"
	//                       },
	//                       "Street": {
	//                         "Value": "Marathonloop"
	//                       },
	//                       "StreetNumber": {
	//                         "Value": "7"
	//                       },
	//                       "PostalCode": {
	//                         "Value": "5235AA"
	//                       },
	//                       "Town": {
	//                         "Value": "'s-Hertogenbosch"
	//                       },
	//                       "Type": {
	//                         "Value": "VEST"
	//                       }
	//                     }
	//                   ]
	//                 }
	//               }
	//             ]
	//           },
	//           "Administrator": {
	//             "Curator": {
	//               "DateStart": {
	//                 "Value": "2020-06-30"
	//               },
	//               "Initials": {
	//                 "Value": "A.B.C."
	//               },
	//               "FamilyName": {
	//                 "Value": "KapotStuk"
	//               },
	//               "Type": {
	//                 "Value": "C"
	//               },
	//               "Adres": {
	//                 "DateStart": {
	//                   "Value": "2020-03-29"
	//                 },
	//                 "Street": {
	//                   "Value": "Postbus"
	//                 },
	//                 "StreetNumber": {
	//                   "Value": "5055"
	//                 },
	//                 "PostalCode": {
	//                   "Value": "5201GB"
	//                 },
	//                 "Town": {
	//                   "Value": "'S-HERTOGENBOSCH"
	//                 },
	//                 "Phone": {
	//                   "Value": "073-123456"
	//                 }
	//               }
	//             }
	//           },
	//           "Publications": {
	//             "Publications": {
	//               "Date": {
	//                 "Value": "2020-07-01"
	//               },
	//               "Code": {
	//                 "Value": "01.obr.17.181.F.1300.3.19"
	//               },
	//               "Description": {
	//                 "Value": "Uitspraak faillissement op 17 juni 2020"
	//               },
	//               "CodeType": {
	//                 "Value": "1300"
	//               },
	//               "HearingLocation": {
	//                 "Street": {
	//                   "Value": "Leeghwaterlaan"
	//                 },
	//                 "StreetNumber": {
	//                   "Value": "8"
	//                 },
	//                 "Town": {
	//                   "Value": "'s-Hertogenbosch"
	//                 }
	//               },
	//               "CourtCaseCode": {
	//                 "Value": "48"
	//               }
	//             }
	//           },
	//           "AvailableReports": {
	//             "Report": [
	//               {
	//                 "Date": {
	//                   "Value": "2020-08-31T16:00:29"
	//                 },
	//                 "Code": {
	//                   "Value": "01_obr_19_183_F_V_02"
	//                 },
	//                 "Title": {
	//                   "Value": "Verslag: 30-07-2020"
	//                 },
	//                 "Final": {
	//                   "Value": "false"
	//                 }
	//               },
	//               {
	//                 "Date": {
	//                   "Value": "2020-09-30T16:00:24"
	//                 },
	//                 "Code": {
	//                   "Value": "01_obr_19_183_F_V_01_B"
	//                 },
	//                 "Title": {
	//                   "Value": "Financieel verslag 30-07-2020"
	//                 },
	//                 "Final": {
	//                   "Value": "false"
	//                 }
	//               },
	//               {
	//                 "Date": {
	//                   "Value": "2021-06-03T14:30:14"
	//                 },
	//                 "Code": {
	//                   "Value": "01_obr_19_184_F_V_03_B"
	//                 },
	//                 "Title": {
	//                   "Value": "Financieel verslag 26-05-2021"
	//                 },
	//                 "Final": {
	//                   "Value": "false"
	//                 }
	//               }
	//             ]
	//           }
	//         }
	//       }
	//     }
	//   }
	// }
}

func ExampleCaseCodes() {
	loadCaseCodesFromXSD()

	codes := []string{}

	for c := range CaseCodes {
		codes = append(codes, c)
	}

	sort.Strings(codes)

	for _, k := range codes {
		fmt.Printf("code %s: description: %s \n", k, CaseCodes[k])
	}
	// Output:

	// Found case codes 119
	// code 1100: description: declaration of bankruptcy in appeal on [date]
	// code 1101: description: bankruptcy de jure as result of interim termination of debt restructuring in appeal on [date]
	// code 1102: description: nullification of bankruptcy in appeal on [date]
	// code 1200: description: declaration of bankruptcy in cassation proceedings on [date]
	// code 1201: description: bankruptcy de jure as result of interim termination of debt restructuring in cassation on [date]
	// code 1202: description: nullification of bankruptcy in cassation on [date]
	// code 1300: description: declaration of bankruptcy on [date]
	// code 1301: description: declaration of bankruptcy as result of interim termination of debt restructuring on [date]
	// code 1302: description: declaration of bankruptcy during debt restructuring on [date]
	// code 1303: description: declaration of bankruptcy by rescission of debt restructuring agreement on [date]
	// code 1304: description: reopening bankruptcy by rescission of bankruptcy agreement on [date]
	// code 1305: description: declaration of bankruptcy by rescission of moratorium agreement on [date]
	// code 1306: description: declaration of bankruptcy after termination of moratorium on [date]
	// code 1307: description: replacement of administrator [name former administrator] by [name new administrator]
	// code 1308: description: simplified bankruptcy proceedings
	// code 1309: description: verification meeting on [date] at [time]
	// code 1310: description: verification meeting and hearing on agreement on [date] at [time]
	// code 1311: description: meeting of creditors on [date] at [time]
	// code 1312: description: deposition of interim distribution list from [date]
	// code 1313: description: deposition of final distribution list from [date]
	// code 1314: description: deposition of final distribution list from [date]
	// code 1315: description: simplified bankruptcy proceedings and deposition of final distribution list from [date]
	// code 1316: description: removal from bankruptcy due to a lack of assets on [date]
	// code 1317: description: termination of bankruptcy by binding distribution list on [date]
	// code 1318: description: termination of bankruptcy by approval of agreement  on [date]
	// code 1319: description: termination of bankruptcy by binding distribution list after opposition on [date]
	// code 1320: description: nullification of bankruptcy after opposition on [date]
	// code 1322: description: filing of discharge request  by mr. [name, address of solicitor]
	// code 1323: description: rectification
	// code 1324: description: termination of bankruptcy because all debts are paid on [date]
	// code 1325: description: pro forma verification meeting
	// code 1326: description: pro forma verification meeting and hearing on agreement
	// code 1328: description: replacement supervisory judge
	// code 1330: description: additional mail blockade
	// code 1331: description: change of address
	// code 1332: description: interim abrogation mail blockade
	// code 1333: description: Bankruptcy converted into debt restructuring on
	// code 1334: description: Bankruptcy transferred to other district court on
	// code 2100: description: admission to debt restructuring in appeal on [date]
	// code 2101: description: conversion of bankruptcy into debt restructuring in appeal on [date]
	// code 2102: description: resurgence of  debt restructuring because of nullification of bankruptcy in appeal on [date]
	// code 2200: description: admission to debt restructuring in cassation on [date]
	// code 2201: description: conversion of bankruptcy into debt restructuring in cassation on [date]
	// code 2202: description: resurgence of debt restructuring because of nullification of bankruptcy in cassation on [date]
	// code 2300: description: rectification
	// code 2301: description: admission to debt restructuring on  [date]
	// code 2302: description: admission to debt restructuring on [date] and verification meeting on [date] at [time]
	// code 2303: description: admission to debt restructuring on [date] and verification meeting with hearing on agreement on [date] at [time]
	// code 2304: description: conversion of bankruptcy into debt restructuring on [date]
	// code 2305: description: admission to debt restructuring  after nullification of bankruptcy by opposition on [date]
	// code 2306: description: conversion of provisional moratorium into debt restructuring on [date]
	// code 2307: description: provisional admission to debt restructuring on [date]
	// code 2308: description: conversion of provisonal admission to debt restructuring into a final admission on [date]
	// code 2309: description: replacement of receiver  [name former receiver] by [name new receiver]
	// code 2310: description: verification meeting on [date] at [time]
	// code 2311: description: verification meeting and hearing on agreement on [date] at [time]
	// code 2312: description: verification meeting, plan of sanitation and hearing on composition on [date] at [time]
	// code 2313: description: resumption of verification meeting, restructuring plan and hearing on agreement on [date] at [time]
	// code 2314: description: verification meeting and hearing on adjustment of restructuring plan on [date] at [time]
	// code 2315: description: verification meeting and hearing on termination on [date] at [time]
	// code 2316: description: verification meeting and hearing on termination on [date] at [time] deposition of final distribution list from [date]
	// code 2317: description: verification meeting and hearing on adjustment of restructuring plan and termination on [date] at [time]
	// code 2318: description: verification meeting and hearing on adjustment of restructuring plan and termination on [date] at [time] deposition of  distribution list from [date]
	// code 2319: description: hearing on agreement on [date] at [time]
	// code 2320: description: hearing on restructuring plan on [date] at [time]
	// code 2321: description: hearing on termination on [date] at [time]
	// code 2322: description: hearing on termination and adjustment of restructuring plan on [date] at [time]
	// code 2323: description: hearing on termination on [date] at [time]  deposition of final distribution list from [date]
	// code 2324: description: hearing on termination and adjustment of restructuring plan on [date] at [time]  deposition of  final distribution list from [date]
	// code 2325: description: meeting of creditors on [date] at [time]
	// code 2326: description: deposition of interim distribution list from [date]
	// code 2327: description: deposition of final distribution list from [date]
	// code 2328: description: termination of debt restructuring because all debts are paid on [date]
	// code 2329: description: termination of debt restructuring because of resumption of payments on [date]
	// code 2330: description: termination of debt restructuring because of approval of agreement on [date]
	// code 2331: description: termination of debt restructuring with distribution and clean slate on [date]
	// code 2332: description: termination of debt restructuring without distribution, with clean slate on [date]
	// code 2333: description: termination of debt restructuring with distribution, without clean slate on [date]
	// code 2334: description: termination of debt restructuring without distribution, without clean slate on [date]
	// code 2335: description: termination of debt restructuring by rejection of final debt restructuring on [date]
	// code 2336: description: deprivation of clean slate in debt restructuring on [date]
	// code 2337: description: nullification of bankruptcy after opposition with resurgence of debt restructuring on [date]
	// code 2338: description: pro forma admission to debt restructuring and verification meeting
	// code 2339: description: pro forma admission to debt restructuring and verification meeting with  hearing on agreement
	// code 2340: description: pro forma verification meeting
	// code 2341: description: pro forma verification meeting and hearing on agreement
	// code 2342: description: pro forma verification meeting, restructuring plan and hearing on agreemeent
	// code 2343: description: pro forma verification meeting and hearing on adjustment of restructuring plan
	// code 2344: description: pro forma verification meeting, hearing on termination
	// code 2345: description: pro forma verification meeting, hearing on termination and deposition of distribution list
	// code 2346: description: pro forma verification meeting and hearing on adjustment of restructuring plan and termination
	// code 2347: description: pro forma verification meeting and hearing on adjustment of restructuring plan and termination and deposition of distribution list from
	// code 2348: description: rendered and accounted
	// code 2349: description: replacement supervisory judge
	// code 2351: description: additional mail blockade
	// code 2352: description: change of address
	// code 2353: description: interim abrogation mail blockade
	// code 2354: description: Premature termination of debt restructuring by declaration of bankruptcy on
	// code 2355: description: Debt restructuring transferred to other district court on
	// code 3100: description: final admission to moratorium in appeal on [date]
	// code 3101: description: hearing court of appeal on rejection of final admission to moratorium on [date] at [time]
	// code 3102: description: hearing court of appeal on final admission to moratorium on [date] at [time]
	// code 3200: description: Granting of final moratorium  in cassation on [date]
	// code 3201: description: proceedings on appeal to the Supreme Court on rejection of final admission to moratorium on [date] at [time]
	// code 3202: description: proceedings on appeal to the Supreme Court  on final admission to moratorium on [date] at [time]
	// code 3300: description: admission to interim moratorium on [date] meeting of creditors on [date] at [time]
	// code 3301: description: admission to interim moratorium on [date] meeting of creditors with hearing on agreement on [date] at [time]
	// code 3302: description: admission to final moratorium by order of [date] from [date] for the duration of [variable number of months / years]
	// code 3303: description: extension of moratorium term by order of [date] from [date] for the duration of  [variable number of months / years]
	// code 3304: description: replacement of receiver  [name former receiver] by [name new receiver]
	// code 3305: description: hearing on extension of moratorium term on [date] at [time]
	// code 3306: description: meeting of creditors with  hearing on agreement on [date] at [time]
	// code 3307: description: termination of moratorium on [date]
	// code 3308: description: termination of moratorium because of expiration of term on [date]
	// code 3309: description: termination of moratorium by approval of agreement on [date]
	// code 3310: description: rectification
	// code 3311: description: replacement supervisory judge
	// code 3313: description: Termination of suspension of payments by conversion into bankruptcy on
	// code 3314: description: Suspension of payments transferred to other district court on
}

func ExampleInsolvencyLocation() {
	out := readTestCase()
	locs := out.AllAddresses()

	for _, l := range locs {
		json, _ := json.MarshalIndent(l.Adres, "", "  ")
		os.Stdout.Write(json)
	}
	// Output:
	// {
	//   "DateStart": {
	//     "Value": "2016-05-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "5"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2016-05-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "5"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2019-04-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "2"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2019-04-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "2"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2017-09-28"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "1"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2017-09-28"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "1"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2017-09-28"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "1"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2019-04-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "7"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }{
	//   "DateStart": {
	//     "Value": "2019-04-24"
	//   },
	//   "Street": {
	//     "Value": "Marathonloop"
	//   },
	//   "StreetNumber": {
	//     "Value": "7"
	//   },
	//   "PostalCode": {
	//     "Value": "5235AA"
	//   },
	//   "Town": {
	//     "Value": "'s-Hertogenbosch"
	//   },
	//   "Type": {
	//     "Value": "VEST"
	//   }
	// }
}
