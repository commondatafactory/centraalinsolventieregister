package soap

import (
	"encoding/xml"
	"fmt"
	"time"
)

type ResponseEnvelope struct {
	XMLName          xml.Name     `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	ResponseBodyBody ResponseBody `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
}

type ResponseBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
	Fault   Fault    `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`
}

type Fault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`
	Code    string   `xml:"faultcode,omitempty"`
	String  string   `xml:"faultstring,omitempty"`
	Actor   string   `xml:"faultactor,omitempty"`
	Detail  string   `xml:"detail,omitempty"`
}

type LastUpdateDate struct {
	Value string `xml:",chardata"`
}

type LastUpdate struct {
	LastUpdateDate *LastUpdateDate `xml:"lastUpdateDate"`
}

type GetLastUpdateResult struct {
	LastUpdate *LastUpdate `xml:"lastUpdate"`
}

type GetLastUpdateResponse struct {
	GetLastUpdateResult *GetLastUpdateResult `xml:"GetLastUpdateResult" json:"GetLastUpdateResult,omitempty" yaml:"GetLastUpdateResult,omitempty"`
}

type OperationGetLastUpdateSoapOut struct {
	GetLastUpdateResponse *GetLastUpdateResponse `xml:"GetLastUpdateResponse" json:"GetLastUpdateResponse,omitempty" yaml:"GetLastUpdateResponse,omitempty"`
}

func (r *OperationGetLastUpdateSoapOut) Result() string {
	date := r.GetLastUpdateResponse.GetLastUpdateResult.LastUpdate.LastUpdateDate.Value
	return date
}

type Insolventienummer struct {
	Value string `xml:",chardata"`
}

type Rechtbank struct {
	Value string `xml:",chardata"`
}

type Date struct {
	Value string `xml:",chardata"`
}

func (p *Date) Parse() time.Time {
	// 2021-06-01T10:50:06
	loc, err := time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		panic(err)
	}

	t, err := time.ParseInLocation("2006-01-02T15:04:05", p.Value, loc)
	if err != nil {
		fmt.Println(err)
		return time.Now()
	}
	return t
}
func (p *Date) ParseShort() time.Time {
	// 2021-06-01
	loc, _ := time.LoadLocation("Europe/Amsterdam")
	t, err := time.ParseInLocation("2006-01-02", p.Value, loc)
	if err != nil {
		fmt.Println(err)
		return time.Now()
	}
	return t
}

type Kenmerk struct {
	Value string `xml:",chardata"`
}

type Titel struct {
	Value string `xml:",chardata"`
}

type Eindverslag struct {
	Value string `xml:",chardata"`
}

type Report struct {
	Insolventienummer *Insolventienummer `xml:"insolventienummer"`
	Rechtbank         *Rechtbank         `xml:"rechtbank"`
	Publicatie        *Date              `xml:"publicatiedatumVerslag"`
	Kenmerk           *Kenmerk           `xml:"kenmerk"`
	Titel             *Titel             `xml:"titel"`
	Eindverslag       *Eindverslag       `xml:"eindverslag"`
}

type BeschikbareVerslagen struct {
	Verslagen []Report `xml:"verslag"`
}

type SearchReportsSinceResult struct {
	BeschikbareVerslagen *BeschikbareVerslagen `xml:"beschikbareVerslagen"`
}

type SearchReportsSinceResponse struct {
	SearchReportsSinceResult *SearchReportsSinceResult `xml:"searchReportsSinceResult,omitempty" json:"searchReportsSinceResult,omitempty" yaml:"searchReportsSinceResult,omitempty"`
}

type OperationSearchReportsSinceSoapOut struct {
	SearchReportsSinceResponse *SearchReportsSinceResponse `xml:"searchReportsSinceResponse,omitempty" json:"searchReportsSinceResponse,omitempty" yaml:"searchReportsSinceResponse,omitempty"`
}

func (reports *OperationSearchReportsSinceSoapOut) ReportsFound() int {
	return len(reports.SearchReportsSinceResponse.SearchReportsSinceResult.BeschikbareVerslagen.Verslagen)
}

func (reports *OperationSearchReportsSinceSoapOut) Reports() []Report {
	return reports.SearchReportsSinceResponse.SearchReportsSinceResult.BeschikbareVerslagen.Verslagen
}

/*
<searchInsolvencyIDResponse xmlns="http://www.rechtspraak.nl/namespaces/cir01">
  <searchInsolvencyIDResult>
     <publicatieLijst xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.rechtspraak.nl/namespaces/inspubber01" extractiedatum="2021-06-07T15:15:58.8675005+02:00">
        <publicatieKenmerk>01.obr.20.183.F.1325.1.21</publicatieKenmerk>
        <publicatieKenmerk>01.obr.20.183.F.1325.1.21</publicatieKenmerk>
     </publicatieLijst>
  </searchInsolvencyIDResult>
</searchInsolvencyIDResponse>
*/

type PublicationCode struct {
	Value string `xml:",chardata"`
}

type PublicationList struct {
	PubliecationCodes []*PublicationCode `xml:"publicatieKenmerk" json:"publicatieKenmerk,omitempty"`
	XSDAttr           string             `xml:"xmlns:xsd,attr,omitempty" json:"xsd,omitempty"`
	DatumAttr         string             `xml:"extractiedatum,attr" json:"extractiedatum"`
}

type SearchInsolvencyIDResult struct {
	PublicationList PublicationList `xml:"publicatieLijst" json:"publicatieLijst,omitempty"`
}

type SearchInsolvencyResponse struct {
	Result SearchInsolvencyIDResult `xml:"searchInsolvencyIDResult,omitempty" json:"Result,omitempty"`
}

type OperationSearchInsolvencyIDResponseSoapOut struct {
	Response SearchInsolvencyResponse `xml:"searchInsolvencyIDResponse,omitempty" json:"Response,omitempty"`
}

func (in *OperationSearchInsolvencyIDResponseSoapOut) GetPublicationList() []*PublicationCode {
	return in.Response.Result.PublicationList.PubliecationCodes
}

type GetCaseWithReportResult struct {
	Value string `xml:",chardata"`
}

type GetCaseWithReportsResponse struct {
	GetCaseWithReportResult *GetCaseWithReportResult `xml:"getCaseWithReportsResult" json:"getCaseWithReportsResult"`
}

type OperationGetCaseReportsResponseSoapOut struct {
	GetCaseWithReportsResponse *GetCaseWithReportsResponse `xml:"getCaseWithReportsResponse" json:"getCaseWithReportsResponse" yaml:"getCaseWithReportsResponse"`
}
