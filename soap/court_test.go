package soap

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"sort"
)

func ExampleSchema() {
	names := []string{}

	for name := range CourtIDs {
		names = append(names, name)
	}
	sort.Strings(names)

	for _, k := range names {
		fmt.Printf("courtid: %s name: %s\n", CourtIDs[k], k)
	}

	// Output:
	// courtid: 09 name: 's-Gravenhage
	// courtid: 01 name: 's-Hertogenbosch
	// courtid: 14 name: Alkmaar
	// courtid: 08 name: Almelo
	// courtid: 13 name: Amsterdam
	// courtid: 05 name: Arnhem
	// courtid: 19 name: Assen
	// courtid: 02 name: Breda
	// courtid: 45 name: Den Haag
	// courtid: 11 name: Dordrecht
	// courtid: 50 name: Gelderland
	// courtid: 18 name: Groningen
	// courtid: 15 name: Haarlem
	// courtid: 17 name: Leeuwarden
	// courtid: 47 name: Limburg
	// courtid: 03 name: Maastricht
	// courtid: 12 name: Middelburg
	// courtid: 42 name: Midden-Nederland
	// courtid: 41 name: Noord-Holland
	// courtid: 43 name: Noord-Nederland
	// courtid: 48 name: Oost-Brabant
	// courtid: 44 name: Oost-Nederland
	// courtid: 51 name: Overijssel
	// courtid: 04 name: Roermond
	// courtid: 10 name: Rotterdam
	// courtid: 16 name: Utrecht
	// courtid: 49 name: Zeeland-West-Brabant
	// courtid: 06 name: Zutphen
	// courtid: 07 name: Zwolle-Lelystad
}

func ExampleSimpleType() {
	s := `
<xs:simpleType name="instantieRechtbankCodePreHGK">
		<xs:restriction base="xs:string">
			<xs:enumeration value="01">
				<xs:annotation>
					<xs:documentation xml:lang="nl">'s-Hertogenbosch</xs:documentation>
					<xs:documentation xml:lang="en">'s-Hertogenbosch</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
</xs:simpleType>
	`

	st := []byte(s)

	marshalStructure := SimpleType{}

	err := xml.Unmarshal(st, &marshalStructure)
	if err != nil {
		panic(err)
	}

	json, _ := json.Marshal(marshalStructure)
	os.Stdout.Write(json)

	// Output:
	// {"XMLName":{"Space":"xs","Local":"simpleType"},"Name":"instantieRechtbankCodePreHGK","Restriction":{"XMLName":{"Space":"xs","Local":"restriction"},"Enumerations":[{"XMLName":{"Space":"xs","Local":"enumeration"},"Code":"01","Annotation":[{"XMLName":{"Space":"xs","Local":"annotation"},"Documentations":[{"XMLName":{"Space":"xs","Local":"documentation"},"Lang":"nl","Value":"'s-Hertogenbosch"},{"XMLName":{"Space":"xs","Local":"documentation"},"Lang":"en","Value":"'s-Hertogenbosch"}]}]}]}}
}

func ExampleEnumeration() {
	s := `
<xs:enumeration value="01">
	<xs:annotation>
		<xs:documentation xml:lang="nl">'s-Hertogenbosch</xs:documentation>
		<xs:documentation xml:lang="en">'s-Hertogenbosch</xs:documentation>
	</xs:annotation>
</xs:enumeration>
	`

	enum := []byte(s)
	marshalStructure := Enumeration{
		Annotation: []Annotation{},
	}

	err := xml.Unmarshal(enum, &marshalStructure)
	if err != nil {
		panic(err)
	}

	json, _ := json.Marshal(marshalStructure)
	os.Stdout.Write(json)

	// Output:
	// {"XMLName":{"Space":"xs","Local":"enumeration"},"Code":"01","Annotation":[{"XMLName":{"Space":"xs","Local":"annotation"},"Documentations":[{"XMLName":{"Space":"xs","Local":"documentation"},"Lang":"nl","Value":"'s-Hertogenbosch"},{"XMLName":{"Space":"xs","Local":"documentation"},"Lang":"en","Value":"'s-Hertogenbosch"}]}]}
}
