
SOAP API CALL for the Centraal Insolventie Register
===================================================


Does xml SOAP calls from and to the centraal insolventie register.

get a fee username password at:  https://insolventies.rechtspraak.nl/#!/registratie

This module crafst and parses the SOAP XML needed to work with the API at:

https://webservice.rechtspraak.nl/cir.asmx

examples

	date, _ := soap.GetLastupdate()
	fmt.Println(date)

	// list new stuff from - to.
	soap.GetLastupdateSince(time.Now().Add(-24*time.Hour), time.Now())

	// lookup the insolvencies per court.
	soap.SearchInsolvencyID("F.01/20/183", "48")

	// load specific details of a case.
	soap.GetCaseWithReports("01.obr.20.183.F.1325.1.21")

	// TODO. Remove old cruft. (6 months old)
	soap.RemovedSince(time.Now().Add(-24*time.Hour))

