module gitlab.com/commondatafactory/centraalinsolventieregister

go 1.16

require (
	github.com/Attumm/settingo v1.4.1
	github.com/hooklift/gowsdl v0.5.0
	gorm.io/datatypes v1.0.2
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.15
)
