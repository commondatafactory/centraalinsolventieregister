#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

REV=`git rev-parse --short HEAD`
export DOCKER_BUILDKIT=1

cd soap
go test
cd ..

docker build -t registry.gitlab.com/commondatafactory/centraalinsolventieregister/cir:$REV$1 .
docker push registry.gitlab.com/commondatafactory/centraalinsolventieregister/cir:$REV$1
