# STEP 1 build binary
FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git
RUN apk --no-cache add ca-certificates

COPY . /app/

WORKDIR /app/cmd/store/
# Fetch dependencies.
RUN go get -d -v
# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build

WORKDIR /app/cmd/apiproxy/
# Fetch dependencies.
RUN go get -d -v
# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build

# STEP 2 build a small image
FROM scratch

COPY docs /app/docs/
# Copy static executables and certificates
COPY --from=builder /app/cmd/ /app/cmd
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /
ENV TZ=Europe/Amsterdam
ENV ZONEINFO=/zoneinfo.zip

WORKDIR /app

CMD /app/cmd/apiproxy/apiproxy
