CREATE EXTENSION fuzzystrmatch;

CREATE TABLE baghelp.pand_buurt_wijk_gemeente_provincie (
    pid character varying(16),
    point cdf.geometry,
    buurtnaam character varying,
    buurtcode character varying,
    wijkcode character varying,
    wijknaam character varying,
    gemeentecode character varying,
    gemeentenaam character varying,
    provincienaam character varying,
    provinciecode character varying
);

create index on baghelp.pand_buurt_wijk_gemeente_provincie (pid);
create index on baghelp.pand_vbo_nums_labels (pid);
create index on baghelp.pand_vbo_nums_labels (postcode);



truncate cir.insolvency_locations_resolved;
insert into cir.insolvency_locations_resolved
select
	pand.*,
	cdf.ST_Transform(pbwgp.point, 3857) as point,
	pbwgp.buurtcode,
	pbwgp.buurtnaam,
	pbwgp.wijkcode,
	pbwgp.wijknaam,
	pbwgp.gemeentenaam,
	pbwgp.gemeentecode,
	pbwgp.provinciecode,
	pbwgp.provincienaam
from (
	select b.huisletter, b.huisnummertoevoeging, il.*, b.pid
	       , levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), concat(lower(addition1), lower(addition2))) tovoeging_score
	from cir.insolvency_locations il,
	lateral ( select pa.pid, pa.huisletter, pa.huisnummertoevoeging
	          from baghelp.pand_vbo_nums_labels pa
	          where pa.postcode = il.postal_code
	          and pa.huisnummer = il.street_number
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), concat(lower(il.addition1), lower(il.addition2))) limit 1
	) b
) pand
left outer join baghelp.pand_buurt_wijk_gemeente_provincie pbwgp on pbwgp.pid = pand.pid
on conflict do nothing;
